package nachos.userprog;

import nachos.machine.*;
import nachos.threads.*;
import nachos.userprog.*;
import java.util.LinkedList;


public class PagePool {
    private LinkedList<Integer> freePagePool;
    // page pool lock
    private Lock poolLock;

    public PagePool(int numPages) {
        // initialize page pool and lock
        poolLock = new Lock();
        freePagePool = new LinkedList<Integer>();
        for (int i = 0; i < numPages; i++) {
            freePagePool.add(new Integer(i));
        }
    }

    /**
     * Acquire physical pages from UserKernel
     */
    public int[] acquirePages(int num) {
        poolLock.acquire();
        if (num > freePagePool.size()) {
            poolLock.release();
            return new int[0];
        }
        else {
            int[] assigned = new int[num];
            for (int i = 0; i < num; i++) {
                assigned[i] = freePagePool.remove().intValue();
            }
            poolLock.release();
            return assigned;
        }
    }

    /**
     * Release physical pages from UserProcess
     */
    public void releasePages(int[] pages) {
        poolLock.acquire();
        for (int i = 0; i < pages.length; i++) {
            freePagePool.add(new Integer(pages[i]));
        }
        poolLock.release();
    }

    /**
     * returns the number of free pages
     */
    public int size() {
        return freePagePool.size(); 
    }
}
