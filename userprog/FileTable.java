package nachos.userprog;

import java.util.ArrayList;

@SuppressWarnings("serial")
public class FileTable<E> {
    private int maxCapacity;
    private int count;
    private ArrayList<E> arr;

    public FileTable(int maxCapacity) {
        this.arr = new ArrayList<E>(maxCapacity);
        this.maxCapacity = maxCapacity;
        this.count = 0;
        
        // set all elements in array to null
        for (int i = 0; i < maxCapacity; i++) {
            arr.add(i, null);
        }
    }

    public E get(int index) {
        return arr.get(index);
    }

    /**
     * Adds an element to the first slot that is not null and
     * returns the index positon of that slot if element is inserted
     */
    public int add(E e) {
        for (int i = 0; i < maxCapacity; i++) {
            if (arr.get(i) == null) {
                set(i, e);
                return i;
            }
        }
        
        throw new ArrayIndexOutOfBoundsException();
    }

    public void remove(int index) {
        set(index, null);
    }

    public int size() {
        return count; 
    }

    public E set(int index, E element) {
        if (arr.get(index) == null && element != null) {
            count++;
        } else if (arr.get(index) != null && element == null) {
            count--;
        }
        return arr.set(index, element);
    }
    
    public boolean full() {
        return count == maxCapacity;
    }
}
