#!/bin/bash

step=$1 || step=0 #Number of steps to apply non-interactively (continues interactively)
[ -z $CLEAN ] && CLEAN=0 #Can set CLEAN environment var to start a fresh local build directory

[ -z $ARCH ] && ARCH=`uname -m`

uname -a
uname -m
uname -s

case "$ARCH" in
x86_64)
  case `uname -s` in
    Darwin) RCH=osx86; ;;
    *) RCH="lin64";
  esac ;;
i686) RCH="lin32"; ;;
sun4u|sun4v) RCH="$ARCH"; ;;
i86pc) RCH="sun86"; ;;
i386) RCH="osx86"; ;;
"Power Macintosh") RCH="osxpp"; ;;
*)
  case `uname -s` in
    CYGWIN*) RCH="cygnt"; ;;
    FreeBSD) RCH="frbsd"; ;;
    *) echo "ARCH: Unrecognized $ARCH and" `uname -s`; exit 1;
  esac
esac

BUILD_LOC=/tmp/build
BUILD_COM=`pwd`/build
FULL_COM=$BUILD_COM/full
ARCH_COM=$BUILD_COM/arch
VER_BUT=2.22
VER_GCC=3.2.2

NAME_BUT=binutils-$VER_BUT
NAME_GCC=gcc-$VER_GCC
TAR_BUT=$NAME_BUT.tar.bz2
TAR_GCC=$NAME_GCC.tar.bz2
PATCH=`which gpatch` || PATCH=`which patch`
PATCH+=" -N -s -u -b"
PATCH_BUT=
PATCH_GCC=$BUILD_COM/ejr-gcc-3.2.2.patch

FULL_RCH=$FULL_COM/$RCH
BIN_RCH=$ARCH_COM/$RCH
HOST_CC="gcc" #Adjusted later
HOST_SHELL=`which ksh` || HOST_SHELL=`which ksh93`
HOST_EXTRA=

MAKE=`which gnumake` || `which gmake` || MAKE=`which make`
MYTAR=`which gtar` || MYTAR=`which tar`
MYTAR+=" --no-same-permissions --no-same-owner -xf"
FETCH=`which fetch` || FETCH=`which wget`
[ -z $FETCH ] && FETCH=`which curl`" -O"

case "$RCH" in
osx86) HOST_CC="/usr/bin/gcc -m32 -mmacosx-version-min=10.6 -arch i686"
	;;
osxpp) HOST_CC="gcc-4.0 -m32 -mmacosx-version-min=10.5 -arch ppc"
	;;
sun*)  HOST_CC=`which /usr/sww/pkg/gcc-3*/bin/gcc`
       export PATH=/usr/sww/pkg/gcc-3*/bin:/usr/ccs/bin:/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/bin:/usr/sww/bin
	;;
lin*)  HOST_CC=`which gcc34`; ;;
frbsd) HOST_CC=`which gcc34`; ;;
cyg*)  HOST_CC=`which gcc-3`; ;;
*) echo "Unrecognized $RCH"; exit 1;
esac

#set

echo "
Basic dependencies (perhaps incomplete list):
  gcc (vers 3/compat) for your platform BUILD/HOST
    (on Mac OS X can be Apple gcc 4)
  binutils for your platform
  gtar (or named tar)
  ksh or ksh93
  bash
  wget or curl (if you want the source auto-fetched)
  gpatch or patch

BUILD $NAME_BUT and $NAME_GCC FOR $RCH
 SRC $BUILD_COM
  TO $FULL_COM
  TO $ARCH_COM
 USING $HOST_CC
 SHELL $HOST_SHELL (extra $HOST_EXTRA)
   TMP $BUILD_LOC
   TAR $MYTAR
 FETCH $FETCH
 FILES $TAR_BUT and $TAR_GCC
  MAKE $MAKE
 PATCH $PATCH
 CLEAN $CLEAN

GCC: " `$HOST_CC -v 2>&1`

(( step-- <= 0 )) && echo "Press any key to continue..." && read


echo "
*** DOWNLOADING to $BUILD_COM if necessary:
"
mkdir -p $BUILD_COM
cd $BUILD_COM
[ -e "$TAR_BUT" ] || $FETCH http://mirrors.kernel.org/gnu/binutils/$TAR_BUT
[ -e "$TAR_GCC" ] || $FETCH http://mirrors.kernel.org/gnu/gcc/$NAME_GCC/$TAR_GCC
mkdir -p $FULL_RCH
mkdir -p $BIN_RCH

echo "
View of $BUILD_COM :"
ls -Al $BUILD_COM
(( step-- <= 0 )) && echo "Press any key to continue..." && read


echo "
*** STAGING in $BUILD_LOC :
"
(( $CLEAN > 0 )) && rm -Rf $BUILD_LOC
mkdir -p $BUILD_LOC
cd $BUILD_LOC
[ -d "$NAME_BUT" ] || $MYTAR $BUILD_COM/$NAME_BUT.tar.bz2
[ -d "$NAME_GCC" ] || $MYTAR $BUILD_COM/$NAME_GCC.tar.bz2
#If Makefile and end product seem present, preserve them
[[ -e "xbinutils/Makefile" && -e "xbinutils/ld/ld-new" ]] || rm -Rf xbinutils 2>/dev/null
[[ -e "xgcc/gcc/Makefile"  && -e "xgcc/gcc/xgcc"       ]] || rm -Rf xgcc      2>/dev/null
mkdir -p xbinutils xgcc

echo "
View of $BUILD_LOC :"
ls -Al $BUILD_LOC
(( step-- <= 0 )) && echo "Press any key to continue..." && read


echo "
*** PATCHING $NAME_GCC ($PATCH_GCC)
"
cd $BUILD_LOC/$NAME_GCC
$PATCH -p1 < $PATCH_GCC

echo "
Patched files:"
ls -l gcc/config/i386/darwin.h* gcc/config.gcc*
(( step-- <= 0 )) && echo "Press any key to continue..." && read


echo "
*** CONFIGURE $NAME_BUT ($HOST_SHELL & $HOST_CC):
"
cd $BUILD_LOC/xbinutils
[ -e Makefile ] || CONFIG_SHELL="$HOST_SHELL" CC="$HOST_CC" ../$NAME_BUT/configure --program-prefix=mips- --prefix=$FULL_RCH --target=mips-dec-ultrix42 --disable-shared --enable-static --disable-nls --disable-werror

head Makefile
(( step-- <= 0 )) && echo "Press any key to continue..." && read


echo "
*** CONFIGURE $NAME_GCC :
"
cd $BUILD_LOC/xgcc
[ -e Makefile ] || CONFIG_SHELL="$HOST_SHELL" CC="$HOST_CC" ../$NAME_GCC/configure --with-gnu-ld --with-gnu-as --program-prefix=mips- --prefix=/tmp/notused --target=mips-dec-ultrix42 --enable-obsolete --disable-multilib --without-headers --disable-shared --enable-static --disable-nls --enable-languages=c++ $HOST_EXTRA
#Use libiberty from the binutils build rather than rebuilding it
rm -Rf libiberty
ln -s ../xbinutils/libiberty .

head Makefile
(( step-- <= 0 )) && echo "Press any key to continue..." && read


echo "
*** BUILD $NAME_BUT :
"
cd $BUILD_LOC/xbinutils
[ -f ld/ld-new ] || $MAKE clean
$MAKE
$MAKE check

echo "
Binutils files:"
file gas/as-new binutils/nm-new ld/ld-new
ld/ld-new -v
(( step-- <= 0 )) && echo "Press any key to continue..." && read


echo "
*** BUILD $NAME_GCC :
"
cd $BUILD_LOC/xgcc
[ -f gcc/xgcc ] || $MAKE clean
$MAKE
#$MAKE check
#Expecting an error here...if it's the write one and the compiler got built, we are ok!

echo "
Compiler files:"
file gcc/cpp0 gcc/cc1 gcc/cc1plus gcc/xgcc gcc/cpp
gcc/xgcc -v
(( step-- <= 0 )) && echo "Press any key to continue..." && read


echo "
*** INSTALL $NAME_BUT :
"
rm -Rf $FULL_RCH
cd $BUILD_LOC/xbinutils
$MAKE install

echo "
VIEW of $FULL_RCH :"
ls -Al $FULL_RCH
(( step-- <= 0 )) && echo "Press any key to continue..." && read


echo "
*** INSTALL $NAME_GCC :
"
#Partial install of gcc (the compiler and pre-processor)
cd $BUILD_LOC/xgcc/gcc
cp cpp0 cc1 cc1plus $FULL_RCH/bin
cp xgcc $FULL_RCH/bin/mips-gcc
cp cpp $FULL_RCH/bin/mips-cpp
cd $FULL_RCH/bin
#Would rather patch GCC so it found "mips-as" but can't figure it out!
ln -s mips-as as

echo "
VIEW of $FULL_RCH :"
ls $FULL_RCH/bin
(( step-- <= 0 )) && echo "Press any key to continue..." && read


echo "
*** COPY bin to $BIN_RCH :
"
cp -R $FULL_RCH/bin/* $BIN_RCH

echo "
FILE in $BIN_RCH :"
file $BIN_RCH/*

echo "
***DONE***
"
