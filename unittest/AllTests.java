package nachos.unittest;

/*
 * This file runs all test suites
 */
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import junit.framework.JUnit4TestAdapter;

@RunWith(Suite.class)
// specify the test suites to run
@SuiteClasses({ 
    nachos.unittest.proj1.AllTests.class, 
    nachos.unittest.proj2.AllTests.class,
}) 

public class AllTests {
    @BeforeClass 
    public static void setUpClass() {      
    }

    @AfterClass 
    public static void tearDownClass() { 
    }

    public static void main(final String[] args) {
        runJUnit();
    }

    public static void runJUnit(){
        System.out.println("Running from runJUnit");
        junit.textui.TestRunner.run (suite());
    }

    public static junit.framework.Test suite() 
    {
       return new JUnit4TestAdapter(AllTests.class);
    }

}
