package nachos.unittest.proj2;

import java.util.*;
import org.junit.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import nachos.machine.*;
import nachos.threads.*;
import nachos.userprog.*;
import java.io.*;

public class PagePoolTest {

    /**
     * acquires and releases the pages
     */
    @Test
    public void simpleTest() {
        int[] expected = { 0, 1, 2, 3, 4 };
        PagePool p = new PagePool(5);

        assertArrayEquals(expected, p.acquirePages(5));
        assertEquals(0, p.size());
        p.releasePages(expected);
        assertEquals(5, p.size());
    }

    /**
     * acquires all the pages, returns two pages back in segmented positions
     * acquires the two pages again
     */
    @Test
    public void interleveTest() {
        int[] expected0 = { 0 };
        int[] expected1 = { 1 };
        int[] expected2 = { 2 };
        int[] expected3 = { 3 };
        int[] expected4 = { 4 };

        PagePool p = new PagePool(5);

        assertArrayEquals(expected0, p.acquirePages(1));
        assertArrayEquals(expected1, p.acquirePages(1));
        assertArrayEquals(expected2, p.acquirePages(1));
        assertArrayEquals(expected3, p.acquirePages(1));
        assertArrayEquals(expected4, p.acquirePages(1));
        p.releasePages(expected2);
        p.releasePages(expected4);

        int[] expected_off = {2, 4};
        assertEquals(2, p.size());
        assertArrayEquals(expected_off, p.acquirePages(2));
        assertEquals(0, p.size());
    }
}
