package nachos.unittest.proj2;

import java.util.*;
import org.junit.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import nachos.machine.*;
import nachos.threads.*;
import java.io.*;
import nachos.userprog.*;

public class VirtualMemoryTest extends UserProcessHelper {
    /**
     * Write bytes and read them back
     *
     * Read VirtualMemory, data should be copied between appropriate ranges;
     * Write VirtualMemory, data should be copied between appropriate ranges.
     */
    @Test
    public void simpleWriteReadTest() {
        byte[] byteList = {0, 1, 2, 3, 4, 5, 6, 7};
        byte[] wrongByteList = {1, 1, 1, 1, 1, 1, 1};
        byte[] blankByteList = new byte[8];

        UserProcess p = createUserProcess();

        assertEquals(byteList.length, p.writeVirtualMemory(0, byteList));

        /* fill up blank byte list when reading */
        assertEquals(byteList.length, p.readVirtualMemory(0, blankByteList));

        /* check */
        assertArrayEquals(byteList, blankByteList);
        assertFalse(Arrays.equals(wrongByteList, blankByteList));
        
        /** try a different index */
        assertEquals(byteList.length, p.writeVirtualMemory(PAGE_SIZE, byteList));

        /* fill up blank byte list when reading */
        assertEquals(byteList.length, p.readVirtualMemory(PAGE_SIZE, blankByteList));

        /* check */
        assertArrayEquals(byteList, blankByteList);
        assertFalse(Arrays.equals(wrongByteList, blankByteList));

        /** try a different index */
        assertEquals(byteList.length, p.writeVirtualMemory(1, byteList));

        /* fill up blank byte list when reading */
        assertEquals(byteList.length, p.readVirtualMemory(1, blankByteList));

        /* check */
        assertArrayEquals(byteList, blankByteList);
        assertFalse(Arrays.equals(wrongByteList, blankByteList));
        
        /* Try to write byteList at the very last vaddr in the page */
        
        /** try a different index */
        assertEquals(1, p.writeVirtualMemory(PAGE_SIZE * NUM_PAGES_PER_PROCESS - 1, byteList));

        /* fill up blank byte list when reading */
        assertEquals(1, p.readVirtualMemory(PAGE_SIZE * NUM_PAGES_PER_PROCESS - 1, blankByteList));
    }

    /**
     * Read and writes arrays of bytes to locations of different lengths at 
     * different virtual address throughout the page table
     */
    @Test
    public void randomReadWriteTest() {
        int maxNum = Machine.processor().pageSize * NUM_PAGES_PER_PROCESS;
        UserProcess p = createUserProcess();

        assertEquals(maxNum, p.writeVirtualMemory(0, new byte[maxNum]));

        for(int i = 0; i < maxNum; i++) 
        {
            assertEquals(maxNum - i, p.writeVirtualMemory(i, new byte[maxNum - i]));
            assertEquals(maxNum - i, p.readVirtualMemory(i, new byte[maxNum - i]));
            assertEquals(i, p.writeVirtualMemory(maxNum - i, new byte[i]));
            assertEquals(i, p.readVirtualMemory(maxNum - i, new byte[i]));
        }
    }
    /**
     * Writes bytes to memory limit and reads them back
     */
    @Test
    public void bigWriteReadTest() {
        int maxNum = Machine.processor().pageSize * NUM_PAGES_PER_PROCESS;
        byte[] byteList = new byte[maxNum];
        byte[] blankByteList = new byte[maxNum];

        UserProcess p = createUserProcess();

        for(int i = 0; i < maxNum; i++) {
            byteList[i] = (new Integer(i % NUM_PAGES_PER_PROCESS)).byteValue();
        }

        assertEquals(byteList.length, p.writeVirtualMemory(0, byteList));

        /* fill up blank byte list when reading */
        assertEquals(byteList.length, p.readVirtualMemory(0, blankByteList));

        /* check */
        assertArrayEquals(byteList, blankByteList); 
    }

    /**
     * Write to the same location x number of times
     */
    @Test
    public void overwriteTest() {
        byte[] byteList = new byte[8];
        byte[] blankByteList = new byte[8];

        UserProcess p = createUserProcess();

        for(int i = 0; i < NUM_PAGES_PER_PROCESS; i++) {
            for(int j = 0; j < byteList.length; j++)
                byteList[j] = (new Integer(i)).byteValue();
            assertEquals(byteList.length, p.writeVirtualMemory(0, byteList));
            assertEquals(byteList.length, p.readVirtualMemory(0, blankByteList));

            /* check */
            assertArrayEquals(byteList, blankByteList); 
        }
    }

    /**
     * Create multiple processes, assert true that none of their virtual memory 
     * pages share the same physical memory page;
     */
    @Test
    public void multipleProcessesTest() {
        byte[] byteList1 = {0, 1,  2,  3,  4,  5,  6,  7};
        byte[] byteList2 = {8, 9, 10, 11, 12, 13, 14, 15};
        byte[] buffer = new byte[byteList1.length];
        UserProcess p1 = createUserProcess();
        UserProcess p2 = createUserProcess();
        
        /* write different values to memory in different process */
        assertEquals(byteList1.length, p1.writeVirtualMemory(0, byteList1));
        assertEquals(byteList2.length, p2.writeVirtualMemory(0, byteList2));
        
        /* check to make sure p1 reads the right memory back */
        assertEquals(byteList1.length, p1.readVirtualMemory(0, buffer));
        assertArrayEquals(byteList1, buffer);

        /* check to make sure p2 reads the right memory back*/
        assertEquals(byteList2.length, p2.readVirtualMemory(0, buffer));
        assertArrayEquals(byteList2, buffer);
    }

    /**
     * Create a process, and exit normally, assert true that all of its virtual 
     * memory pages are in free page list;
     */

    /**
     * Create a process, and force it to exit when running, assert true that all
     * of its virtual memory pages are in free page list;
     */

    /**
     * Run UserProcess.loadSections(), assert equal of the number of allocated 
     * physical pages and number of pages required for the process;
     */

    /**
     * Create some large processes to use out the physical pages, then create a
     * new process and execute, assert true that an error is reported;
     */
    @Test
    public void useAllPagesTest() {
        /* bigProcesses uses up all the memroy */
        UserProcess bigProcess = createUserProcess(Machine.processor().getNumPhysPages());
        UserProcess smallProcess = createUserProcess();

        int maxNum = Machine.processor().pageSize * Machine.processor().getNumPhysPages();
        byte[] byteList1 = new byte[maxNum];
        byte[] byteList2 = new byte[maxNum];
       
        /* make bigProcess use up all of memory */
        assertEquals(byteList1.length, bigProcess.writeVirtualMemory(0, byteList1));

        /* smallProcess should not be able to write since out of memory */
        /*
         * This has to be tested with a real program.
        */
    }

    /**
     * Read/write VirtualMemory with invalid virtual address, should do nothing 
     * and return 0;
     */
    @Test
    public void invalidVirtualAddressTest() {
        UserProcess p = createUserProcess(); 
        assertEquals(0, p.writeVirtualMemory(-1, new byte[1]));
        assertEquals(0, p.writeVirtualMemory(MAX_NUM_BYTES, new byte[1]));
        assertEquals(0, p.readVirtualMemory(-1, new byte[1]));
        assertEquals(0, p.readVirtualMemory(MAX_NUM_BYTES, new byte[1]));
    }

    /**
     * Read VirtualMemory with an invalid page, should stop at that page and 
     * return number of bytes that have been transferred;
     */
    @Test
    public void readInvalidPageTest() {
        UserProcess p = createUserProcess(); 
        TranslationEntry[] pageTable = p.getPageTable();
        pageTable[1].valid = false;
        assertEquals(PAGE_SIZE, p.readVirtualMemory(0, new byte[PAGE_SIZE * 2]));
    }

    /**
     * Write VirtualMemory with a read only page, should stop at that page and 
     * return number of bytes that have been transferred;
     */
    @Test
    public void writeReadOnlyPageTest() {
        UserProcess p = createUserProcess(); 
        TranslationEntry[] pageTable = p.getPageTable();
        pageTable[1].readOnly = true;
        assertEquals(PAGE_SIZE, p.writeVirtualMemory(0, new byte[PAGE_SIZE * 2]));
    }

    /**
     * Write VirtualMemory with an invalid page, should stop at that page and 
     * return number of bytes that have been transferred;
     */
    @Test
    public void writeInvalidPageTest() {
        UserProcess p = createUserProcess(); 
        TranslationEntry[] pageTable = p.getPageTable();
        pageTable[1].valid = false;
        assertEquals(PAGE_SIZE, p.writeVirtualMemory(0, new byte[PAGE_SIZE * 2]));
    }


    /**
     * Read VirtualMemory, all pages involved should be set to used;
     */
    @Test
    public void usedVirtualMemoryTest() {
        UserProcess p = createUserProcess(); 
        TranslationEntry[] pageTable = p.getPageTable();
        assertEquals(PAGE_SIZE * pageTable.length, p.readVirtualMemory(0, new byte[PAGE_SIZE * pageTable.length]));

        for(int i = 0; i < pageTable.length; i++)
            assertEquals(true, pageTable[i].used);
    }

    /**
     * Write VirtualMemory, all pages involved should be set to used and dirty;
     */
    @Test
    public void dirtyVirtualMemoryTest() {
        UserProcess p = createUserProcess(); 
        TranslationEntry[] pageTable = p.getPageTable();
        assertEquals(PAGE_SIZE * pageTable.length, p.writeVirtualMemory(0, new byte[PAGE_SIZE * pageTable.length]));

        for(int i = 0; i < pageTable.length; i++)
            assertEquals(true, pageTable[i].dirty);
    }

}
