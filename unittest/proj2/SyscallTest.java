package nachos.unittest.proj2;

import java.util.*;
import org.junit.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import nachos.machine.*;
import nachos.threads.*;
import java.io.*;
import nachos.userprog.*;

public class SyscallTest extends UserProcessHelper {
    
    /**
     * Creats a file name in a UserProcess's memory at vaddr
     */
    public void createFileName(String s, UserProcess p, int vaddr) {
        byte[] name = new byte[s.length() + 1];
        char[] charArr = s.toCharArray();

        for (int i = 0; i < s.length(); i++)
            name[i] = (byte) charArr[i];
        name[s.length()] = 0;
        p.writeVirtualMemory(vaddr, name);
    }
    
    /**
     * handleCreate()
     * create a file with an invalid filename (as described in FileName)
     *
     * Creates a file verifies that the file has been created
     * and then deletes the file
     */
    @Test
    public void handleCreateTest() {
        int namePtr = 0; // address of the file name
        UserProcess p = createUserProcess();
        createFileName("test.txt", p, namePtr);
        assertTrue(p.handleCreate(namePtr) > -1);
        assertTrue(p.handleUnlink(namePtr) > -1);
    }

    /**
     * create over 16 files and verify the 17th file cannot be created
     * open a file when the number of files open is 16 (this is also taken care 
     * of)
     */
    @Test
    public void createLimitTest() {
        UserProcess p = createUserProcess();
        int namePtr = 0; // address of the file name
        int[] namePtrArr = new int[UserProcess.maxOpenFiles];
        int numFiles = UserProcess.maxOpenFiles - 2; // -2 because one needed 
                                                     // for stdin and stdout

        for(int i = 0; i < numFiles; i++)
        {
            String fileName = "test" + i + ".txt";
            createFileName(fileName, p, namePtr);
            assertTrue(p.handleCreate(namePtr) > -1);
            namePtrArr[i] = namePtr;
            namePtr += fileName.length() + 1;
        }

        createFileName("badfile.txt", p, namePtr);
        assertEquals(-1, p.handleCreate(namePtr));

    
        for(int i = 0; i < numFiles; i++)
        {
            assertTrue(p.handleUnlink(namePtrArr[i]) > -1);
        }
    }

    /**
     * open a file with an invalid name
     */
    @Test
    public void openInvalidFileNameTest() {
        UserProcess p = createUserProcess();
        assertEquals(-1, p.handleOpen(3));
    }

    /**
     * handleRead()
     * read a file with an invalid file descriptor
     * read a file when the file pointer is right before EOF
     */
    @Test
    public void readInvalidFileDescriptorTest() {
        UserProcess p = createUserProcess();
        assertEquals(-1, p.handleRead(3, 0, 1));
        assertEquals(-1, p.handleRead(-1, 0, 1));
        assertEquals(-1, p.handleRead(100, 0, 1));
        assertEquals(-1, p.handleRead(3, 0, -1));
        assertEquals(-1, p.handleRead(-1, 0, -1));
        assertEquals(-1, p.handleRead(100, 0, -1));
    }

    /**
     * handleWrite()
     * write a file with an invalid file descriptor
     * write to a file when the disk is full
     */
    @Test
    public void writeInvalidFileDescriptorTest() {
        UserProcess p = createUserProcess();
        assertEquals(-1, p.handleWrite(3, 0, 1));
        assertEquals(-1, p.handleWrite(-1, 0, 1));
        assertEquals(-1, p.handleWrite(100, 0, 1));
        assertEquals(-1, p.handleWrite(3, 0, -1));
        assertEquals(-1, p.handleWrite(-1, 0, -1));
        assertEquals(-1, p.handleWrite(100, 0, -1));
    }

    /**
     * handleClose()
     * close a file descriptor that doesn’t exist
     */
    @Test
    public void closeInvalidFileDescriptorTest() {
        UserProcess p = createUserProcess();
        assertEquals(-1, p.handleClose(3));
        assertEquals(-1, p.handleClose(-1));
        assertEquals(-1, p.handleClose(100));
    }
    /**
     * handleUnlink()
     * unlink an invalid file that doesn’t exist
     */
    @Test
    public void unlinkInvalidFileDescriptorTest() {
        UserProcess p = createUserProcess();
        assertEquals(-1, p.handleUnlink(3));
        assertEquals(-1, p.handleUnlink(-1));
        assertEquals(-1, p.handleUnlink(100));
    }

    /**
     * open, write to file, close file, open file, read file, 
     * verifies whats written in is also what is read
     */
    @Test
    public void fileReadWriteTest() {
        int namePtr = 0; // address of the file name
        UserProcess p = createUserProcess();
        String fileName = "test.txt";
        createFileName(fileName, p, namePtr);
        byte[] fileContents = {1, 2, 3, 4, 0};
        byte[] buffer = new byte[fileContents.length];

        int fileContentsPtr = 20;
        p.writeVirtualMemory(fileContentsPtr, fileContents);

        int filePtr = p.handleCreate(namePtr);
        assertTrue(filePtr > -1);
        assertEquals(fileContents.length, p.handleWrite(filePtr, fileContentsPtr, fileContents.length));
        assertEquals(0, p.handleClose(filePtr));

        filePtr = p.handleOpen(namePtr);
        int bufferPtr = 40;
        assertEquals(fileContents.length, p.handleRead(filePtr, bufferPtr, buffer.length));

        p.readVirtualMemory(bufferPtr, buffer);
        assertArrayEquals(fileContents, buffer);

        assertTrue(p.handleUnlink(namePtr) > -1);
    }

}
