package nachos.unittest.proj2;

/*
 * Example test suite
 */
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(value = Suite.class)

@SuiteClasses(value = { 
    // specify test cases in a suite
    FileTableTest.class,
    PagePoolTest.class,
    VirtualMemoryTest.class,
    SyscallTest.class,
    LotterySchedulerTest.class, 
}) 

public class AllTests {
}
