package nachos.unittest.proj2;

import java.util.*;
import org.junit.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import nachos.machine.*;
import nachos.threads.*;
import java.io.*;
import nachos.userprog.*;

/**
 * This is helper class for functions that need to use a UserProcess
 *
 * To use this class, extend it and add tests to it.
 */

public class UserProcessHelper {
    public static final int NUM_PAGES_PER_PROCESS = 4;
    public static final int PAGE_SIZE = Machine.processor().pageSize;
    public static final int MAX_NUM_BYTES = 
        Machine.processor().getNumPhysPages() * PAGE_SIZE;

    private ArrayList<UserProcess> processList;

    @Before
    public void setUp() {
        processList = new ArrayList<UserProcess>();
    }

    @After
    public void tearDown() {
        for( UserProcess p : processList)
            destroyUserProcess(p);

        assertEquals(Machine.processor().getNumPhysPages(), UserKernel.freePagePool.size());
    }

    /**
     * setup page table
     */
    public UserProcess createUserProcess() {
        return createUserProcess(NUM_PAGES_PER_PROCESS);
    }

    public UserProcess createUserProcess(int numPages) {
	    int[] assigned = UserKernel.freePagePool.acquirePages(numPages);

        if (assigned.length == 0) {
            return null;
        }

        TranslationEntry[] pageTable = new TranslationEntry[numPages];

        for (int i = 0; i < numPages; i++) {
            pageTable[i] = new TranslationEntry(i, assigned[i], true, false, false, false);
        }

        UserProcess p = new UserProcess();
        p.setPageTable(pageTable);
        p.setNumPages(numPages);
        processList.add(p);
        return p;
    }

    public void destroyUserProcess(UserProcess p) {
        int numPages = p.getNumPages();
        TranslationEntry[] pageTable = p.getPageTable();
        int[] toBeRelease = new int[numPages];
        for (int i = 0; i < numPages; i++) {
            pageTable[i].valid = false;
            toBeRelease[i] = pageTable[i].ppn;
        }
        UserKernel.freePagePool.releasePages(toBeRelease);
    }
}
