package nachos.unittest.proj2;

import org.junit.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import nachos.machine.*;
import nachos.threads.*;
import java.io.*;

public class LotterySchedulerTest extends LotteryScheduler {
	
    private static Scheduler defaultScheduler;
    
    @BeforeClass
    public static void oneTimeSetup() {
        // Save current scheduler and set scheduler to priortiy scheduler
        defaultScheduler = ThreadedKernel.scheduler;
        ThreadedKernel.scheduler = new LotteryScheduler();    
    }
    
    @AfterClass
    public static void oneTimeTearDown() {
        // restore default scheduler
        ThreadedKernel.scheduler = defaultScheduler;
    }
    
    @Test
    public void testAcquireNewQueue() {
    	test1();	
    }
    
    @Test
    public void testSetPriority() {
    	test2();
    }
    
    @Test
    public void testWaitNewThread() {
    	test3();
    }
    
    @Test
    public void testRelease() {
    	test4();
    }

    @Test
    public void testMultipleQueues() {
    	test5();
    }
    
    @Test
    public void testMultipleThreads() {
    	test6();
    }
    
    @Test
    public void testTansitivity() {
    	test7();
    }
    
    @Test
    public void testSetPriorityOnQueue() {
    	test8();
    }

    @Test
    public void testNoDonation() {
	test9();
    }
}
