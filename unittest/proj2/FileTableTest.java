package nachos.unittest.proj2;

import org.junit.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import nachos.machine.*;
import nachos.threads.*;
import nachos.userprog.*;
import java.util.ArrayList;
import java.io.*;

public class FileTableTest {
    @Test
    public void addTest() {
        // Simple add test
        FileTable<String> t = new FileTable<String>(4);
        t.set(0, "a");
        t.set(2, "c");
        assertEquals("a", t.get(0));
        assertEquals(null, t.get(1));
        assertEquals("c", t.get(2));
        assertEquals(null, t.get(3));
        assertEquals(2, t.size());
    }

    @Test
    public void add2Test() {
        // Add an element when the index 1 element is null
        FileTable<String> t = new FileTable<String>(4);
        t.set(0, "a");
        t.set(2, "c");
        t.set(3, "d");
        assertEquals(3, t.size());
        t.add("b");

        assertEquals("a", t.get(0));
        assertEquals("b", t.get(1));
        assertEquals("c", t.get(2));
        assertEquals("d", t.get(3));
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void addOverflowTest() {
        FileTable<String> t = new FileTable<String>(4);
        t.add("a");
        t.add("b");
        t.add("c");
        t.add("d");
        t.add("e");
    }
    
    @Test
    public void checkFullTest() {
        FileTable<String> t = new FileTable<String>(4);
        t.add("a");
        assertTrue(!t.full());
        t.add("b");
        assertTrue(!t.full());
        t.add("c");
        assertTrue(!t.full());
        t.add("d");
        assertTrue(t.full());
        try {
            t.add("e");
        } catch (Exception e) { }
        assertTrue(t.full());
        try {
            t.add("f");
        } catch (Exception e) { }
        assertTrue(t.full());
        try {
            t.add("g");
        } catch (Exception e) { }
        assertTrue(t.full());
        t.remove(0);
        assertTrue(!t.full());
        t.add("h");
        assertTrue(t.full());
    }

    @Test
    public void addFullTest() {
        FileTable<String> t = new FileTable<String>(4);
        t.set(0, "a");
        t.set(1, "b");
        t.set(2, "c");
        t.set(3, "d");
        assertEquals(4, t.size());

        t.set(0, "a");
        t.set(1, "b");
        t.set(2, "c");
        t.set(3, "d");
        assertEquals(4, t.size());

        assertEquals("a", t.get(0));
        assertEquals("b", t.get(1));
        assertEquals("c", t.get(2));
        assertEquals("d", t.get(3));
    }

    @Test
    public void addRandomTest() {
        FileTable<String> t = new FileTable<String>(4);
        t.set(0, "a");
        t.set(2, "c");
        t.add("b");
        assertEquals("b", t.get(1));
        t.add("d");
        assertEquals("d", t.get(3));
    }

    @Test
    public void setTest() {
        FileTable<String> t = new FileTable<String>(4);
        t.set(0, "a");
        t.set(1, "b");
        t.set(2, "c");
        t.set(3, "d");
        t.set(0, null);
        t.set(1, null);
        t.set(2, null);
        t.set(3, null);

        assertEquals(0, t.size());
        assertEquals(null, t.get(0));
        assertEquals(null, t.get(1));
        assertEquals(null, t.get(2));
        assertEquals(null, t.get(3));
    }

    @Test
    public void removeTest() {
        FileTable<String> t = new FileTable<String>(4);
        t.set(0, "a");
        t.set(1, "b");
        t.set(2, "c");
        t.set(3, "d");

        t.remove(0);
        assertEquals(null, t.get(0));
        assertEquals("b", t.get(1));
        assertEquals("c", t.get(2));
        assertEquals("d", t.get(3));

        t.remove(1);
        assertEquals(null, t.get(0));
        assertEquals(null, t.get(1));
        assertEquals("c", t.get(2));
        assertEquals("d", t.get(3));

        t.remove(2);
        assertEquals(null, t.get(0));
        assertEquals(null, t.get(1));
        assertEquals(null, t.get(2));
        assertEquals("d", t.get(3));

        t.remove(3);
        assertEquals(null, t.get(0));
        assertEquals(null, t.get(1));
        assertEquals(null, t.get(2));
        assertEquals(null, t.get(3));
    }
}

