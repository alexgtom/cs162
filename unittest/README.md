Unit Tests
==========

This is the directory where all the unit tests will be stored. Every unit test 
belongs to a test suite. Checkout the following files for examples:
 * ```proj1/SampleTest.java``` -- Example of a test case
 * ```proj1/AllTests.java``` -- Example of a test suite
 * ```AllTests.java``` -- Runs a set of test suites. The ```Makefile``` runs 
   this file.

Testing Tools
-------------
 * [JUnit4](https://github.com/dsaff/junit) -- Unit test framework
 * [mockitto](http://code.google.com/p/mockito/) -- For stubbing and mocking 
   functionality.

How to run unit tests
---------------------
Run ```make``` in this directory

How to run small boat test
--------------------
smallboattest only runs a subset of the cases from boattest
Run ```make smallboattest```

How to run boat test
--------------------
Run ```make boattest```

How to add unit tests
---------------------
To add a unit test, create a file in named ```*Test.java``` in proj* and 
add the test case to the ```@SuiteClasses``` array in  ```proj*/AllTests.java```

How to run nachos in this directory
-----------------------------------
You can just do ```nachos``` this directory or ```make nachos```. If you do 
```nachos```, make sure you have a symlink to ```nachos.conf``` from the 
correct project directory. ```make nachos``` automatically takes care of 
the symlink determined by the ```PROJ``` in the ```Makefile```

MIPS Cross Compiler
-------------------
Cross-compilers are setup for Mac OS X and Linux. Just run ```make``` in 
the ```test``` directory.
