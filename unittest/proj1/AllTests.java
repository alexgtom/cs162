package nachos.unittest.proj1;

/*
 * Example test suite
 */
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(value = Suite.class)

@SuiteClasses(value = { 
    // specify test cases in a suite
    AlarmTest.class,
    SampleTest.class, 
    KThreadTest.class, 
    BoatCheckerTest.class,
    CommunicatorTest.class,
    PrioritySchedulerTest.class,
    IslandTest.class,
    Condition2Test.class,
}) 

public class AllTests {
}
