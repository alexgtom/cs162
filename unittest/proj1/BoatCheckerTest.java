
package nachos.unittest.proj1;

import org.junit.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import nachos.machine.*;
import nachos.threads.*;
import nachos.ag.BoatGrader;
import java.util.ArrayList;
import java.lang.Exception;
import java.io.*;
import nachos.unittest.BoatChecker;

public class BoatCheckerTest {

    @Test(expected = Exception.class)
    public void invalidSequence1Test() throws Exception {
        // Try to row to molokai twice
        BoatChecker bg = new BoatChecker(0, 2);
        bg.ChildRowToMolokai();
        bg.ChildRideToMolokai();
        bg.ChildRowToMolokai();
        bg.check();
    }

    @Test(expected = Exception.class)
    public void invalidSequence2Test() throws Exception {
        // Try to put more than two children on a boat
        BoatChecker bg = new BoatChecker(0, 3);
        bg.ChildRowToMolokai();
        bg.ChildRideToMolokai();
        bg.ChildRideToMolokai();
        bg.check();
    }

    @Test(expected = Exception.class)
    public void invalidSequence3Test() throws Exception {
        // Send two adults over
        BoatChecker bg = new BoatChecker(2, 0);
        bg.AdultRowToMolokai();
        bg.AdultRowToMolokai();
        bg.check();
    }

    @Test(expected = Exception.class)
    public void invalidSequence4Test() throws Exception {
        // Send adult over from wrong island
        BoatChecker bg = new BoatChecker(1, 0);
        bg.AdultRowToOahu();
        bg.check();
    }

    @Test(expected = Exception.class)
    public void invalidSequence5Test() throws Exception {
        // Ride before you row
        BoatChecker bg = new BoatChecker(2, 0);
        bg.ChildRideToMolokai();
        bg.ChildRowToMolokai();
        bg.check();
    }

    @Test
    public void validSequence1Test() throws Exception {
        BoatChecker bg = new BoatChecker(0, 2);
        bg.ChildRowToMolokai();
        bg.ChildRideToMolokai();
        bg.check();
    }
    
    @Test
    public void validSequence2Test() throws Exception {
        BoatChecker bg = new BoatChecker(2, 2);
        bg.ChildRowToMolokai();
        bg.ChildRideToMolokai();
        bg.ChildRowToOahu();
        bg.AdultRowToMolokai();
        bg.ChildRowToOahu();
        bg.ChildRowToMolokai();
        bg.ChildRideToMolokai();
        bg.ChildRowToOahu();
        bg.AdultRowToMolokai();
        bg.ChildRowToOahu();
        bg.ChildRowToMolokai();
        bg.ChildRideToMolokai();
        bg.check();
    }

    @Test
    public void validSequence3Test() throws Exception {
        BoatChecker bg = new BoatChecker(0, 10);
        bg.ChildRowToMolokai();
        bg.ChildRideToMolokai();
        bg.ChildRowToOahu();
        bg.ChildRowToMolokai();
        bg.ChildRideToMolokai();
        bg.ChildRowToOahu();
        bg.ChildRowToMolokai();
        bg.ChildRideToMolokai();
        bg.ChildRowToOahu();
        bg.ChildRowToMolokai();
        bg.ChildRideToMolokai();
        bg.ChildRowToOahu();
        bg.ChildRowToMolokai();
        bg.ChildRideToMolokai();
        bg.ChildRowToOahu();
        bg.ChildRowToMolokai();
        bg.ChildRideToMolokai();
        bg.ChildRowToOahu();
        bg.ChildRowToMolokai();
        bg.ChildRideToMolokai();
        bg.ChildRowToOahu();
        bg.ChildRowToMolokai();
        bg.ChildRideToMolokai();
        bg.ChildRowToOahu();
        bg.ChildRowToMolokai();
        bg.ChildRideToMolokai();
        bg.ChildRowToOahu();
        bg.check();
    }
}
