package nachos.unittest.proj1;

import org.junit.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import nachos.machine.*;
import nachos.threads.*;
import nachos.ag.BoatGrader;
import java.util.ArrayList;
import java.lang.Exception;
import java.io.*;
import nachos.unittest.BoatChecker;

public class IslandTest {
    @Test
    public void IslandTest() {
        Island Oahu = new Island("Oahu");
        Island Oahu2 = new Island("Oahu");

        assertEquals(0, Oahu.numAdults);
        Oahu.incNumAdults();
        assertEquals(1, Oahu.numAdults);
        Oahu.decNumAdults();
        assertEquals(0, Oahu.numAdults);
        assertEquals(0, Oahu.numChildren);
        Oahu.incNumChildren();
        assertEquals(1, Oahu.numChildren);
        Oahu.decNumChildren();
        assertEquals(0, Oahu.numChildren);
        assertEquals(Oahu, Oahu2);
    }

    public static void runCheckerTest(int adults, int children) throws Exception {
        // run test
        BoatChecker bg = new BoatChecker(adults, children);
        Boat.begin(adults, children, bg);
        bg.check();
    }
}
