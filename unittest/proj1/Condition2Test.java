package nachos.unittest.proj1;

import org.junit.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import nachos.machine.*;
import nachos.threads.*;
import java.io.*;
import java.lang.reflect.Field;

public class Condition2Test {
    Condition2 cond;
    Lock l;
    boolean b;
    int count;

    @Test
    public void condition2Test() throws Exception {
        l = new Lock();
        cond = new Condition2(l);
        b = false;

        KThread t1 = new KThread(new Runnable() {
            public void run() {
                l.acquire();
                cond.sleep();
                b = true;
                l.release();
            }
        });
        KThread t2 = new KThread(new Runnable() {
            public void run() {
                l.acquire();
                cond.wake();
                l.release();
            }
        });
        t1.fork();
        t2.fork();

        t1.join();
        t2.join();

        assertTrue(b);
    
    }

    @Test
    public void wakeAllTest() throws Exception {
        l = new Lock();
        cond = new Condition2(l);
        count = 0;


        KThread t1 = new KThread(new Runnable() {
            public void run() {
                l.acquire();
                cond.sleep();
                count++;
                l.release();
            }
        });

        KThread t2 = new KThread(new Runnable() {
            public void run() {
                l.acquire();
                cond.sleep();
                b = true;
                count++;
                l.release();
            }
        });

        KThread t3 = new KThread(new Runnable() {
            public void run() {
                l.acquire();
                cond.wakeAll();
                l.release();
            }
        });
        t1.fork();
        t2.fork();
        t3.fork();

        t1.join();
        t2.join();
        t3.join();

        assertEquals(2, count);
    
    }
}
