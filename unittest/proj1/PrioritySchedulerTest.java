package nachos.unittest.proj1;

import org.junit.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import nachos.machine.*;
import nachos.threads.*;
import java.io.*;
import java.util.ArrayList;


public class PrioritySchedulerTest extends PriorityScheduler {
    private static Scheduler defaultScheduler;
    @BeforeClass
    public static void oneTimeSetup() {
        // Save current scheduler and set scheduler to priortiy scheduler
        defaultScheduler = ThreadedKernel.scheduler;
        ThreadedKernel.scheduler = new PriorityScheduler();    
    }

    @AfterClass
    public static void oneTimeTearDown() {
        // restore default scheduler
        ThreadedKernel.scheduler = defaultScheduler;
    }

    @Test
    public void testAcquireNewQueue() {
	test1();
    }

    @Test
    public void testSetPriority() {
	test2();
    }

    @Test
    public void testWaitNewThread() {
	test3();
    }

    @Test
    public void fifoTest() {
	test4();
    }
    
    @Test
    public void testRelease() {
	test5();
    }

    @Test
    public void testMultipleThreads() {
	test6();
    }

    @Test
    public void testMultipleQueues() {
	test7();
    }

    @Test
    public void testTansitivity() {
	test8();
    }
    
    @Test
    public void internalTest() {
	selfTest();
    }

    @Test
    public void randomTest() {
	test9();
    }


}



