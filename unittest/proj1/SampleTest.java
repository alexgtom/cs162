/*
 * Example test cases
 */
package nachos.unittest.proj1;

import org.junit.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import nachos.machine.*;

import java.util.*;

public class SampleTest {
    @Test
    public void testEmptyCollection() {
        Collection collection = new ArrayList();
        assertTrue(collection.isEmpty());
        
        // Example of using OpenFile from the nachos package
        OpenFile o = new OpenFile();

        // Example of using mockito
        List mockedList = mock(List.class);
    }
}
