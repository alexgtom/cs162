package nachos.unittest.proj1;

import org.junit.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import nachos.machine.*;
import nachos.threads.*;
import java.util.ArrayList;
import java.io.*;

public class AlarmTest {
    /**
    * SeqThread that can store the name of the thread and how long it is supposed to sleep.
    * It is designed to test that multiple threads could wake up in the right sequence.
    * It has a class variable wakeSeq that keeps track the sequence of the threads' waking up time.
    */
    private static class SeqThread implements Runnable {
            char Name;
            long SleepTicks;

            static String wakeSeq = "";
            static Lock lock = new Lock();

            public SeqThread(char name, long ticks) {
                Name = name;
                SleepTicks = ticks;
            }

            public void run() {
                ThreadedKernel.alarm.waitUntil(SleepTicks);
                lock.acquire();
                wakeSeq = wakeSeq + Name;
                lock.release();
            }
    }
    
    /*
     * This test is freezing jUnit for some reason
     */
    @Ignore
    @Test
    public void AlarmTest() {
        long ticks;
        Alarm test = new Alarm();
        for (int i = 0; i<3; i++) {
            ticks= (long) (Math.random()*1000000);
            long sleepTime = Machine.timer().getTime();
            test.waitUntil(ticks);
            long wakeTime = Machine.timer().getTime();
            assertTrue(wakeTime-sleepTime >= ticks);
        }
    }
    @Test
    public void sequenceTest() {
        
        KThread threadA = new KThread(new SeqThread('A',100));
        KThread threadB = new KThread(new SeqThread('B',800));
        KThread threadC = new KThread(new SeqThread('C',1500));
        KThread threadD = new KThread(new SeqThread('D',2200));
        threadA.fork();
        threadB.fork();        
        threadC.fork();
        threadD.fork();
        threadA.join();
        threadB.join();
        threadC.join();
        threadD.join();
        assertEquals("ABCD", SeqThread.wakeSeq);
    }
}
