package nachos.unittest.proj1;

import org.junit.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import nachos.machine.*;
import nachos.threads.*;
import java.io.*;
import java.lang.reflect.Field;

public class KThreadTest {
    private static KThread a;
    private static KThread b;

    @Test 
    public void threadJoin1Test() {
        //  Tries a join on thread x before x actually runs

        a = new KThread(new Runnable() {
            public void run() {
                b = new KThread(new Runnable() {
                    public void run() {
                    }
                });
                b.join();
                Lib.assertTrue(4 != b.getStatus());
            }
        });
        a.fork();
        
    }

    @Test
    public void threadJoin2Test() {
        //  Tries a join on thread x before x actually runs
        a = new KThread(new Runnable() {
            public void run() {
                b = new KThread(new Runnable() {
                    public void run() {
                    }
                });
                b.fork();
                b.join();
                b.join();
                assertTrue(4 == b.getStatus());
            }
        });
        a.fork();
        
    }
    
    private static Lock l;
    private static int count;
    @Test
    public void threadJoin3Test() {
        l = new Lock(); 
        count = 0;
        
        for(int i = 0; i < 200; i++)
        {
            KThread t1 = new KThread(new Runnable() {
                public void run() {
                    l.acquire();
                    count++;
                    l.release();
                } 
            });
            t1.fork();
            t1.join();
            assertEquals(i+1, count);
        }
    }

    /**
     * This sampleTest is the implementation of KThread.selfTest refactored
     * into this class
     */

    private static final char dbgThread = 't';
    private static KThread currentThread = null;
    private static String output = "";
    
    @Ignore
    @Test
    public void sampleTest() {
        // capture stdout
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        PrintStream old = System.out;
        System.setOut(ps);
        
        // run test
        Lib.debug(dbgThread, "Enter KThreadTest.sampleTest");
        currentThread = new KThread(new PingTest(1));
        currentThread.setName("forked thread");
        currentThread.fork();
        new PingTest(0).run();

        assertEquals(
                "*** thread 0 looped 0 times\n"
                + "*** thread 1 looped 0 times\n"
                + "*** thread 0 looped 1 times\n"
                + "*** thread 1 looped 1 times\n"
                + "*** thread 0 looped 2 times\n"
                + "*** thread 1 looped 2 times\n"
                + "*** thread 0 looped 3 times\n"
                + "*** thread 1 looped 3 times\n"
                + "*** thread 0 looped 4 times\n"
                + "*** thread 1 looped 4 times\n", baos.toString());

        // Restore stdout back
        System.out.flush();
        System.setOut(old);
    }

    private static class PingTest implements Runnable {
	PingTest(int which) {
	    this.which = which;
	}
	
	public void run() {
	    for (int i=0; i<5; i++) {
		System.out.println("*** thread " + which + " looped "+ i + " times");
		currentThread.yield();
	    }
	}

	private int which;
    }
}
