package nachos.unittest.proj1;
import java.util.*;
import org.junit.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import nachos.machine.*;
import nachos.threads.*;
import java.io.*;

public class CommunicatorTest {
	
	static LinkedList<Integer> list = new LinkedList();
	static LinkedList<Integer> list2 = new LinkedList();
	static boolean error = false;
	@Test
	public void sampleTest() {
		//running two listeners then two speakers
		Communicator communicator = new Communicator();
		CommunicatorThreads s1 = new CommunicatorThreads(communicator, 1, true);
		CommunicatorThreads s2 = new CommunicatorThreads(communicator, 2, true);
		CommunicatorThreads l1 = new CommunicatorThreads(communicator, 3, false);
		CommunicatorThreads l2 = new CommunicatorThreads(communicator, 3, false);
		KThread t1 = new KThread(l1);
        KThread t2 = new KThread(l2);
        KThread t3 = new KThread(s1);
        KThread t4 = new KThread(s2);
        t1.fork();
        t2.fork();
        t3.fork();
        t4.fork();
        t1.join();
        t2.join();
        t3.join();
        t4.join();
        assertEquals(list, new LinkedList<Integer>());
        //Running two speakers then two listeners
        CommunicatorThreads s3= new CommunicatorThreads(communicator, 3, true);
		CommunicatorThreads s4= new CommunicatorThreads(communicator, 9, true);
		CommunicatorThreads l3= new CommunicatorThreads(communicator, 3, false);
		CommunicatorThreads l4= new CommunicatorThreads(communicator, 3, false);
        KThread t5 = new KThread(s4);
        KThread t6 = new KThread(s3);
        KThread t7 = new KThread(l3);
        KThread t8 = new KThread(l4);
       
        t5.fork();
        t6.fork();
        t7.fork();
        t8.fork();
        t8.join();
        t7.join();
        t6.join();
        t5.join();
        assertEquals(list, new LinkedList<Integer>());
        s1 = new CommunicatorThreads(communicator, 2, true);
        s2 = new CommunicatorThreads(communicator, 3, true);
        s3 = new CommunicatorThreads(communicator, 99, true);
        s4 = new CommunicatorThreads(communicator, 5, true);
        l1 = new CommunicatorThreads(communicator, 0, false);
        l2 = new CommunicatorThreads(communicator, 0, false);
        l3 = new CommunicatorThreads(communicator, 0, false);
        l4 = new CommunicatorThreads(communicator, 0, false);
        t1 = new KThread(s1);
        t2 = new KThread(s2);
        t3 = new KThread(s3);
        t4 = new KThread(s4);
        t5 = new KThread(l1);
        t6 = new KThread(l2);
        t7 = new KThread(l3);
        t8 = new KThread(l4);
        
        t1.fork();
        t6.fork();
        t2.fork();
        t7.fork();
        t4.fork();
        t3.fork();
        t5.fork();
        t8.fork();
       
        t8.join();
        t6.join();
        t1.join();
        t2.join();
        t7.join();
        t5.join();
        t3.join();
        t4.join();
        assertEquals(list, new LinkedList<Integer>());

        
	}


private static class CommunicatorThreads implements Runnable {
	private Communicator c;
	private int message;
	private boolean speaker;
    public CommunicatorThreads (Communicator c, int i, boolean speaker) {
	    this.c = c;
	    if(speaker){
	    this.message = i;}
	    this.speaker = speaker;
	    
	}
    
	public void run() {
		//means it is running a speaker.
	    if (this.speaker){
	    	list.add(this.message);
	    	c.speak(this.message);
	    }else{
	    	int temp = c.listen();
	    	list.remove(new Integer(temp));
	    	
	    }
	}
}

}