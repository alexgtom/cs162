#!/bin/bash
proj=proj1
nachos=../bin/nachos
#config=../$proj/roundrobin.conf
config=../$proj/priorityscheduler.conf
adultlimit=248
childlimit=248
threadlimit=250
for adult in $(seq 0 $adultlimit)
do
    for child in $(seq 2 $childlimit)
    do
        sum=`expr $adult + $child`

        if (( sum < threadlimit - 1 ))
        then
            $nachos -- nachos.ag.BoatCheckerGrader -c $child -a $adult -[] $config
        fi
    done
done
