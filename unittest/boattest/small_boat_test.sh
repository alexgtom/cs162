#!/bin/bash
proj=proj1
nachos=../bin/nachos
roundrobinconfig=../$proj/roundrobin.conf
priorityconfig=../$proj/priorityscheduler.conf

# round robin
config=$roundrobinconfig
#$nachos -- nachos.ag.BoatCheckerGrader -c 10 -a 0 -[] $config

config=$priorityconfig
# priority scheduler
$nachos -- nachos.ag.BoatCheckerGrader -c 38 -a 0 -[] $config -d t
#$nachos -- nachos.ag.BoatCheckerGrader -c 8 -a 0 -[] $config
#$nachos -- nachos.ag.BoatCheckerGrader -c 10 -a 0 -[] $config
#$nachos -- nachos.ag.BoatCheckerGrader -c 11 -a 0 -[] $config
#$nachos -- nachos.ag.BoatCheckerGrader -c 12 -a 0 -[] $config
#$nachos -- nachos.ag.BoatCheckerGrader -c 14 -a 0 -[] $config
#$nachos -- nachos.ag.BoatCheckerGrader -c 15 -a 0 -[] $config
#$nachos -- nachos.ag.BoatCheckerGrader -c 16 -a 0 -[] $config
#$nachos -- nachos.ag.BoatCheckerGrader -c 18 -a 0 -[] $config
#$nachos -- nachos.ag.BoatCheckerGrader -c 19 -a 0 -[] $config
#$nachos -- nachos.ag.BoatCheckerGrader -c 20 -a 0 -[] $config
#$nachos -- nachos.ag.BoatCheckerGrader -c 21 -a 0 -[] $config
#$nachos -- nachos.ag.BoatCheckerGrader -c 22 -a 0 -[] $config
#$nachos -- nachos.ag.BoatCheckerGrader -c 23 -a 0 -[] $config
#$nachos -- nachos.ag.BoatCheckerGrader -c 24 -a 0 -[] $config
#$nachos -- nachos.ag.BoatCheckerGrader -c 30 -a 0 -[] $config
#$nachos -- nachos.ag.BoatCheckerGrader -c 40 -a 0 -[] $config
#$nachos -- nachos.ag.BoatCheckerGrader -c 41 -a 0 -[] $config
#$nachos -- nachos.ag.BoatCheckerGrader -c 46 -a 0 -[] $config
