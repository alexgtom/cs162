package nachos.unittest;
import  org.junit.Assert.*;
import nachos.ag.BoatGrader;
import nachos.machine.*;
import nachos.threads.*;
import java.util.ArrayList;

/*
 * Grammer: 
 *
 * begin: oahu 
 * oahu: (child_row_molokai | ADULT_ROW_TO_MOLOKAI) molokai?
 * child_row_molokai: CHILD_ROW_TO_MOLOKAI (CHILD_RIDE_TO_MOLOKAI)?
 * molokai: (child_row_oahu | ADULT_ROW_TO_OAHU) oahu?
 * child_row_oahu: CHILD_ROW_TO_OAHU (CHILD_RIDE_TO_OAHU)?
 */

public class BoatChecker extends BoatGrader {
    public enum Action {
        CHILD_ROW_TO_MOLOKAI("CHILD_ROW_TO_MOLOKAI"), 
        CHILD_ROW_TO_OAHU("CHILD_ROW_TO_OAHU"),
        CHILD_RIDE_TO_MOLOKAI("CHILD_RIDE_TO_MOLOKAI"),
        CHILD_RIDE_TO_OAHU("CHILD_RIDE_TO_OAHU"),
        ADULT_ROW_TO_MOLOKAI("ADULT_ROW_TO_MOLOKAI"), 
        ADULT_ROW_TO_OAHU("ADULT_ROW_TO_OAHU"),
        ADULT_RIDE_TO_MOLOKAI("ADULT_RIDE_TO_MOLOKAI"),
        ADULT_RIDE_TO_OAHU("ADULT_RIDE_TO_OAHU");

        private Action(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }

        private final String text;
    };

    private ArrayList<Action> sequence;

    public int numChildrenAtMolokai;
    public int numAdultsAtMolokai;
    public int numChildrenAtOahu;
    public int numAdultsAtOahu;

    public final boolean DEBUG = false;

    public BoatChecker(int adult, int children) throws Exception {
        sequence = new ArrayList<Action>();
    }


    private  void begin(ArrayList<Action> tokens) throws Exception {
        debug("begin");
        debugTokens(tokens);

        oahu(tokens); 
        if(tokens.size() > 0)
            throw new Exception("Expected no tokens left but got: " + tokens.toString());
    }
    
    private  void oahu(ArrayList<Action> tokens) throws Exception {
        debug("oahu");
        debugTokens(tokens);

        if (tokens.get(0).equals(Action.CHILD_ROW_TO_MOLOKAI))
        {
            childRowMolokai(tokens);
        }
        else if (tokens.get(0).equals(Action.ADULT_ROW_TO_MOLOKAI))
        {
            numAdultsAtMolokai++;
            numAdultsAtOahu--;
            tokens.remove(0);
        }
        else
            throw new Exception("Expected CHILD_ROW_TO_MOLOKAI or ADULT_ROW_TO_MOLOKAI");

        if(tokens.size() == 0)
            return;
        
        
        molokai(tokens);
    }

    private  void molokai(ArrayList<Action> tokens) throws Exception {
        debug("molokai");
        debugTokens(tokens);

        
        if (tokens.size() == 0)
            return;

        if (tokens.get(0).equals(Action.CHILD_ROW_TO_OAHU))
            childRowOahu(tokens);
        else if (tokens.get(0).equals(Action.ADULT_ROW_TO_OAHU))
        {
            numAdultsAtOahu++;
            numAdultsAtMolokai--;
            tokens.remove(0);
        }
        else
        {
            throw new Exception("Expected CHILD_ROW_TO_OAHU or ADULT_ROW_TO_OAHU");
        }
        
        if(tokens.size() == 0)
            return;
        
        oahu(tokens);
    }

    private  void childRowMolokai(ArrayList<Action> tokens) throws Exception {
        debug("child_row_molokai");
        debugTokens(tokens);

        if (tokens.get(0).equals(Action.CHILD_ROW_TO_MOLOKAI))
        {
            numChildrenAtMolokai++;
            numChildrenAtOahu--;
            tokens.remove(0);
        }
        else
            throw new Exception("Expected CHILD_ROW_TO_MOLOKAI");

        
        if(tokens.size() == 0)
            return;

        if (tokens.get(0).equals(Action.CHILD_RIDE_TO_MOLOKAI))
        {
            numChildrenAtMolokai++;
            numChildrenAtOahu--;
            tokens.remove(0);
        }
    }

    private  void childRowOahu(ArrayList<Action> tokens) throws Exception {
        debug("child_row_oahu");
        debugTokens(tokens);

        
        if (tokens.get(0).equals(Action.CHILD_ROW_TO_OAHU))
        {
            numChildrenAtOahu++;
            numChildrenAtMolokai--;
            tokens.remove(0);
        }
        else
            throw new Exception("Expected CHILD_ROW_TO_OAHU");

        if(tokens.size() == 0)
            return;
        

        if (tokens.get(0).equals(Action.CHILD_RIDE_TO_OAHU))
        {
            numChildrenAtOahu++;
            numChildrenAtMolokai--;
            tokens.remove(0);
        }
         
    }


    public  void debug(String s) {
        if (DEBUG)
            System.err.println(s);
    }

    public  void debugTokens(ArrayList<Action> s) {
        if (DEBUG) 
        {
            System.err.println(s.size() + "  " +s.toString());
        }
    }

    /**
     * BoatGrader consists of functions to be called to show that
     * your solution is properly synchronized. This version simply
     * prints messages to standard out, so that you can watch it.
     * You cannot submit this file, as we will be using our own
     * version of it during grading.

     * Note that this file includes all possible variants of how
     * someone can get from one island to another. Inclusion in
     * this class does not imply that any of the indicated actions
     * are a good idea or even allowed.
     */


    /* ChildRowToMolokai should be called when a child pilots the boat
       from Oahu to Molokai */
    public void ChildRowToMolokai() {
        sequence.add(Action.CHILD_ROW_TO_MOLOKAI);
    }

    /* ChildRowToOahu should be called when a child pilots the boat
       from Molokai to Oahu*/
    public void ChildRowToOahu() {
        sequence.add(Action.CHILD_ROW_TO_OAHU);
    }

    /* ChildRideToMolokai should be called when a child not piloting
       the boat disembarks on Molokai */
    public void ChildRideToMolokai() {
        sequence.add(Action.CHILD_RIDE_TO_MOLOKAI);
    }

    /* ChildRideToOahu should be called when a child not piloting
       the boat disembarks on Oahu */
    public void ChildRideToOahu() {
        sequence.add(Action.CHILD_RIDE_TO_OAHU);
    }

    /* AdultRowToMolokai should be called when a adult pilots the boat
       from Oahu to Molokai */
    public void AdultRowToMolokai() {
        sequence.add(Action.ADULT_ROW_TO_MOLOKAI);
    }

    /* AdultRowToOahu should be called when a adult pilots the boat
       from Molokai to Oahu */
    public void AdultRowToOahu() {
        sequence.add(Action.ADULT_ROW_TO_OAHU);
    }

    /* AdultRideToMolokai should be called when an adult not piloting
       the boat disembarks on Molokai */
    public void AdultRideToMolokai() {
        sequence.add(Action.ADULT_RIDE_TO_MOLOKAI);
    }

    /* AdultRideToOahu should be called when an adult not piloting
       the boat disembarks on Oahu */
    public void AdultRideToOahu() {
        sequence.add(Action.ADULT_RIDE_TO_OAHU);
    }

    public void check() throws Exception {
        begin(sequence);

    }

    public void run() {
        int adults = 0;
        int children = 1;
        try {
            BoatChecker bg = new BoatChecker(adults, children);
            Boat.begin(adults, children, bg);
            bg.check();
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.exit(1);
        }
    
    }
    
    public static void runCheckerTest(int adults, int children) throws Exception {
        // run test
        BoatChecker bg = new BoatChecker(adults, children);
        Boat.begin(adults, children, bg);
        bg.check();
    }
}





