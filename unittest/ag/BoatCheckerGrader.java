package nachos.ag;

import nachos.unittest.BoatChecker;
import nachos.machine.*;

public class BoatCheckerGrader extends AutoGrader {
    void run() {
        int adult = Machine.boatCheckerNumAdults;
        int child = Machine.boatCheckerNumChildren;

        System.err.println("****************           # adults: " + adult + "    # children: " + child);

        try {
            BoatChecker.runCheckerTest(adult, child);
        }
        catch (Exception e) {
            System.err.println(e.getMessage());
            System.exit(1);
        }
        kernel.run();
        kernel.terminate();
    }
}
