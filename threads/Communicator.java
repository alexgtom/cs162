package nachos.threads;

import nachos.machine.*;

/**
 * A <i>communicator</i> allows threads to synchronously exchange 32-bit
 * messages. Multiple threads can be waiting to <i>speak</i>,
 * and multiple threads can be waiting to <i>listen</i>. But there should never
 * be a time when both a speaker and a listener are waiting, because the two
 * threads can be paired off at this point.
 */
public class Communicator {
    /**
     * Allocate a new communicator.
     */
	private boolean bufferUsed;
	private int bufferMsg;
	private Condition speakerCondition;//Waiting for listener to finish
	private Condition listenerCondition;
	private Condition speakerWaitingForListener; //When speaker is finished transferring, waiting for listeners to receive messages
	private Lock lock;
    public Communicator() {
    	lock = new Lock();
    	bufferMsg = -1;
    	bufferUsed= false;
    	speakerCondition= new Condition(lock);
    	listenerCondition= new Condition(lock);
    	speakerWaitingForListener = new Condition(lock);
    	
    }

    /**
     * Wait for a thread to listen through this communicator, and then transfer
     * <i>word</i> to the listener.
     *
     * <p>
     * Does not return until this thread is paired up with a listening thread.
     * Exactly one listener should receive <i>word</i>.
     *
     * @param	word	the integer to transfer.
     */
    public void speak(int word) {
    	lock.acquire();
    	while(bufferUsed){
    		speakerCondition.sleep();
    	}
    	bufferMsg = word;
    	bufferUsed= true;
    	listenerCondition.wake();
    	if(bufferUsed){
    		speakerWaitingForListener.sleep();
    	}
    	lock.release();
    }

    /**
     * Wait for a thread to speak through this communicator, and then return
     * the <i>word</i> that thread passed to <tt>speak()</tt>.
     *
     * @return	the integer transferred.
     */    
    public int listen() {
    	lock.acquire();
    	while(!bufferUsed){
    		listenerCondition.sleep();
    	}
    	int message = bufferMsg;
    	bufferUsed = false;
    	speakerWaitingForListener.wake();
    	speakerCondition.wake();
    	lock.release();
    	return message;
	
    }
    
}
