package nachos.threads;

import nachos.machine.*;

/**
 * An implementation of condition variables that disables interrupt()s for
 * synchronization.
 *
 * <p>
 * You must implement this.
 *
 * @see	nachos.threads.Condition
 */
public class Condition2 {
    /**
     * Allocate a new condition variable.
     *
     * @param	conditionLock	the lock associated with this condition
     *				variable. The current thread must hold this
     *				lock whenever it uses <tt>sleep()</tt>,
     *				<tt>wake()</tt>, or <tt>wakeAll()</tt>.
     */
    public Condition2(Lock conditionLock) {
	this.conditionLock = conditionLock;
	
	// Initialize waitQueue
	this.waitQueue = ThreadedKernel.scheduler.newThreadQueue(false);
    }

    /**
     * Atomically release the associated lock and go to sleep on this condition
     * variable until another thread wakes it using <tt>wake()</tt>. The
     * current thread must hold the associated lock. The thread will
     * automatically reacquire the lock before <tt>sleep()</tt> returns.
     */
    public void sleep() {
	Lib.assertTrue(conditionLock.isHeldByCurrentThread());
	
	// Disable interrupts
	boolean intStatus = Machine.interrupt().disable();
	
	conditionLock.release();

	// Put currentThread onto waitQueue and put it into sleep
	KThread currentThread = KThread.currentThread();
	this.waitQueue.waitForAccess(currentThread);
	currentThread.sleep();

	conditionLock.acquire();
	
	// Restore interrupts
	Machine.interrupt().restore(intStatus);
    }

    /**
     * Wake up at most one thread sleeping on this condition variable. The
     * current thread must hold the associated lock.
     */
    public void wake() {
	Lib.assertTrue(conditionLock.isHeldByCurrentThread());
	
	// Disable interrupts
	boolean intStatus = Machine.interrupt().disable();
	
	// Wake up the first thread on the waitQueue
	KThread nextThread = this.waitQueue.nextThread();
	if (nextThread != null) {
		nextThread.ready();
	}
	
	// Restore interrupts
	Machine.interrupt().restore(intStatus);
    }

    /**
     * Wake up all threads sleeping on this condition variable. The current
     * thread must hold the associated lock.
     */
    public void wakeAll() {
	Lib.assertTrue(conditionLock.isHeldByCurrentThread());
	
	// Disable interrupts
	boolean intStatus = Machine.interrupt().disable();
	
	// Wake up all the threads on the waitQueue
	KThread nextThread = this.waitQueue.nextThread();
	while (nextThread != null) {
		nextThread.ready();
		nextThread = this.waitQueue.nextThread();
	}
	
	// Restore interrupts
	Machine.interrupt().restore(intStatus);
    }

    private Lock conditionLock;
    
    // Instance variable for condition variables
    private ThreadQueue waitQueue;
}
