package nachos.threads;
import nachos.ag.BoatGrader;

public class Boat {
    static BoatGrader bg;
    enum Location {OAHU, MOLOKAI};

    static Lock operationLock;
    static Condition2 passenger;
    static Condition2 childrenAtOahu;
    static Condition2 adultsAtOahu;
    static Condition2 childrenAtMolokai;
    static Condition2 adultsAtMolokai;
    static Island boatLocation;
    static int passengerOnBoat;
    static Island oahu;
    static Island molokai;
    static Communicator c;

    public static void selfTest() {
        BoatGrader b = new BoatGrader();

        System.out.println("\n ***Testing Boats with only 2 children***");
        begin(0, 2, b);

        System.out.println("\n ***Testing Boats with 2 children, 1 adult***");
        begin(1, 2, b);

        System.out.println("\n ***Testing Boats with 3 children, 3 adults***");
        begin(3, 3, b);
    }

    public static void begin( int adults, int children, BoatGrader b ) {
        // Store the externally generated autograder in a class
        // variable to be accessible by children.
        bg = b;

        // Instantiate global variables
        oahu = new Island("Oahu");
        molokai = new Island("Molokai");
        operationLock = new Lock();
        passenger = new Condition2(operationLock);
        childrenAtOahu = new Condition2(operationLock);
        adultsAtOahu = new Condition2(operationLock);
        childrenAtMolokai = new Condition2(operationLock);
        adultsAtMolokai = new Condition2(operationLock);
        boatLocation = oahu;
        passengerOnBoat = 0;
        c = new Communicator();

        // Create and fork children and adults threads
        operationLock.acquire();

        Runnable r = null;
        
        for(int i = 0; i < children; i++) {
            r = new Runnable() {
                public void run() {
                    ChildItinerary();
                }
            };
            KThread t = new KThread(r);
            t.setName("Child-" + i);
            t.fork();
        }
        
        for(int i = 0; i < adults; i++) {
            r = new Runnable() {
                public void run() {
                    AdultItinerary();
                }
            };
            KThread t = new KThread(r);
            t.setName("Adult-" + i);
            t.fork();
        }

        operationLock.release();
        
        // Check for termination of simulation
        int result = 0;
        while (result != children + adults) {
            
            // Listen to the communicator
            result = c.listen();
            
            // False alert, wake up a child on Molokai and continue the simulation
            if (result != children + adults) {
                
                operationLock.acquire();
                childrenAtMolokai.wake();
                operationLock.release();
            }

        }
    }

    static void AdultItinerary() {
        
        // Set initial location
        Island myLocation = oahu;
        
        // Increment number of adults on Oahu
        operationLock.acquire();
        oahu.incNumAdults();
        
        // If boat is not in Oahu, there are more than 1 child on Oahu,
        // or if the boat is not empty, the adult sleeps on Oahu
        while (boatLocation.equals(molokai) || oahu.getNumChildren() > 1 ||
                passengerOnBoat != 0) {
            adultsAtOahu.sleep();

        }
        
        // Leave Oahu and goes to Molokai
        oahu.decNumAdults();
        passengerOnBoat++;
        boatLocation = molokai;
        bg.AdultRowToMolokai();
        passengerOnBoat--;
        myLocation = molokai;
        molokai.incNumAdults();
        
        // Wake up a child on Molokai to send the boat back to Oahu
        childrenAtMolokai.wake();
        
        // Sleep on Molokai
        adultsAtMolokai.sleep();
        
        // This should never be reached since adults are never woken up
        operationLock.release();
    }

    static void ChildItinerary() {
        
        // Set up initial location and finished flag

        Island myLocation = oahu;
        boolean finished = false;

        // Increment number of children on Oahu
        operationLock.acquire();
        oahu.incNumChildren();

        while(true) {
            
            if (myLocation.equals(oahu)) {
                
                // If the child is on Oahu, and the boat is not at Oahu, or the
                // boat is full, the child sleeps on Oahu
                while (boatLocation.equals(molokai) || passengerOnBoat > 1) {
                    childrenAtOahu.sleep();

                }

                // When the child is awake, the boat is not full
                if (passengerOnBoat == 0) {
                    
                    // Leave Oahu as a passenger

                    oahu.decNumChildren();
                    
                    // If there is only one child left on Oahu, he/she must be the
                    // driver and the simulation should be terminated
                    if (oahu.getNumAdults() == 0 && oahu.getNumChildren() <= 1)
                    {
                        finished = true;
                    }
                    
                    passengerOnBoat++;
                    
                    // Sleep on the boat, waiting for the driver
                    passenger.sleep();
                    
                    // Arrive on Molokai when waken up
                    bg.ChildRideToMolokai();
                    passengerOnBoat--;
                    myLocation = molokai;
                    molokai.incNumChildren();
                    
                } else if (passengerOnBoat == 1) {
                    
                    // Leave Oahu as a driver, with a passenger on the boat

                    oahu.decNumChildren();
                    passengerOnBoat++;
                    
                    // Drive the boat to Molokai
                    boatLocation = molokai;
                    bg.ChildRowToMolokai();
                    
                    // Arrive at Molokai and wake up the passenger
                    passengerOnBoat--;
                    myLocation = molokai;
                    passenger.wake();
                    molokai.incNumChildren();
                    
                    // Sleep on Molokai
                    childrenAtMolokai.sleep();
                }
            } else {
                if (finished) {
                    
                    // Signal begin() when the child thinks the simulation should
                    // be terminated
                    c.speak(molokai.getNumAdults() + molokai.getNumChildren());

                    childrenAtMolokai.sleep();
                    
                    // If it is a false alert, the child will be waken up
                    finished = false;
                }
                
                // Leave Molokai for Oahu to resume the simulation

                molokai.decNumChildren();
                passengerOnBoat++;
                boatLocation = oahu;
                bg.ChildRowToOahu();
                passengerOnBoat--;
                myLocation = oahu;
                oahu.incNumChildren();
                
                // Wake up another child if there are more than 1 child on Oahu
                if (oahu.getNumChildren() > 1) {
                    childrenAtOahu.wake(); 
                    
                // Otherwise wake up an adult and sleep on Oahu
                } else {
                    adultsAtOahu.wake();
                    childrenAtOahu.sleep();
                }
            }
        }
    }

    static void SampleItinerary() {
        // Please note that this isn't a valid solution (you can't fit
        // all of them on the boat). Please also note that you may not
        // have a single thread calculate a solution and then just play
        // it back at the autograder -- you will be caught.
        System.out.println("\n ***Everyone piles on the boat and goes to Molokai***");
        bg.AdultRowToMolokai();
        bg.ChildRideToMolokai();
        bg.AdultRideToMolokai();
        bg.ChildRideToMolokai();
    }

    static void debug(String s) {
        System.err.println(KThread.currentThread().getName() + ": " + s);
    }
}
