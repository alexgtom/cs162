package nachos.threads;

import nachos.machine.*;

import java.util.TreeSet;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * A scheduler that chooses threads based on their priorities.
 *
 * <p>
 * A priority scheduler associates a priority with each thread. The next thread
 * to be dequeued is always a thread with priority no less than any other
 * waiting thread's priority. Like a round-robin scheduler, the thread that is
 * dequeued is, among all the threads of the same (highest) priority, the
 * thread that has been waiting longest.
 *
 * <p>
 * Essentially, a priority scheduler gives access in a round-robin fassion to
 * all the highest-priority threads, and ignores all other threads. This has
 * the potential to
 * starve a thread if there's always a thread waiting with higher priority.
 *
 * <p>
 * A priority scheduler must partially solve the priority inversion problem; in
 * particular, priority must be donated through locks, and through joins.
 */
public class PriorityScheduler extends Scheduler {
    /**
     * Allocate a new priority scheduler.
     */
    public PriorityScheduler() {
    }
    
    /**
     * Allocate a new priority thread queue.
     *
     * @param	transferPriority	<tt>true</tt> if this queue should
     *					transfer priority from waiting threads
     *					to the owning thread.
     * @return	a new priority thread queue.
     */
    @Override
    public ThreadQueue newThreadQueue(boolean transferPriority) {
	PriorityQueue queue =  new PriorityQueue(transferPriority);
	return queue;
    }

    @Override
    public int getPriority(KThread thread) {
	Lib.assertTrue(Machine.interrupt().disabled());
		       
	return getThreadState(thread).getPriority();
    }

    @Override
    public int getEffectivePriority(KThread thread) {
	Lib.assertTrue(Machine.interrupt().disabled());
		       
	return getThreadState(thread).getEffectivePriority();
    }

    @Override
    public void setPriority(KThread thread, int priority) {
	Lib.assertTrue(Machine.interrupt().disabled());
		       
	Lib.assertTrue(priority >= priorityMinimum &&
		   priority <= priorityMaximum);
	
	getThreadState(thread).setPriority(priority);
    }

    @Override
    public boolean increasePriority() {
	boolean intStatus = Machine.interrupt().disable();
		       
	KThread thread = KThread.currentThread();

	int priority = getPriority(thread);
	if (priority == priorityMaximum)
	    return false;

	setPriority(thread, priority+1);

	Machine.interrupt().restore(intStatus);
	return true;
    }

    @Override
    public boolean decreasePriority() {
	boolean intStatus = Machine.interrupt().disable();
		       
	KThread thread = KThread.currentThread();

	int priority = getPriority(thread);
	if (priority == priorityMinimum)
	    return false;

	setPriority(thread, priority-1);

	Machine.interrupt().restore(intStatus);
	return true;
    }

    /**
     * The default priority for a new thread. Do not change this value.
     */
    public static final int priorityDefault = 1;
    /**
     * The minimum priority that a thread can have. Do not change this value.
     */
    public static final int priorityMinimum = 0;
    /**
     * The maximum priority that a thread can have. Do not change this value.
     */
    public static final int priorityMaximum = 7;    

    /**
     * Return the scheduling state of the specified thread.
     *
     * @param	thread	the thread whose scheduling state to return.
     * @return	the scheduling state of the specified thread.
     */
    protected ThreadState getThreadState(KThread thread) {
	if (thread.schedulingState == null)
	    thread.schedulingState = new ThreadState(thread);

	return (ThreadState) thread.schedulingState;
    }

    public static int qid = 0;

    /**
     * A <tt>ThreadQueue</tt> that sorts threads by priority.
     */
    protected class PriorityQueue extends ThreadQueue {
	PriorityQueue(boolean transferPriority) {
	    this.transferPriority = transferPriority;
	    id = qid;
	    queue = new java.util.PriorityQueue<ThreadState>(255, new ThreadCompare());
	    value = 0;
	    owner = null;
	    qid++;
	}

	@Override
	public void waitForAccess(KThread thread) {
	    Lib.assertTrue(Machine.interrupt().disabled());
	    getThreadState(thread).waitForAccess(this);
	}

	@Override
	public void acquire(KThread thread) {
	    Lib.assertTrue(Machine.interrupt().disabled());
	    getThreadState(thread).acquire(this);
	}

	@Override
	public KThread nextThread() {
	    Lib.assertTrue(Machine.interrupt().disabled());
	    // implement me
	    if (queue.size() != 0) {
		ThreadState t = queue.peek();
		if (owner != null) {
		    ThreadState oldOwner = owner;
		    oldOwner.donors.remove(this);
		    owner = null;
		    if (oldOwner.donors.size() != 0) {
			if (oldOwner.effectivePriority > Math.max(oldOwner.priority, oldOwner.donors.peek().value)) {
			    oldOwner.updateEffectivePriority(Math.max(oldOwner.priority, oldOwner.donors.peek().value));
			}
		    }
		    else {
			if (oldOwner.effectivePriority > oldOwner.priority) {
			    oldOwner.updateEffectivePriority(oldOwner.priority);
			}
		    }
		}
		queue.remove(t);
		t.waiting = null;
		t.time = (long) -1;
		updateValue();
		t.acquire(this);
		return t.thread;
	    }
	    else {
		if (owner != null) {
		    owner.donors.remove(this);
		    owner = null;
		}
		return null;
	    }
	}

	/**
	 * Return the next thread that <tt>nextThread()</tt> would return,
	 * without modifying the state of this queue.
	 *
	 * @return	the next thread that <tt>nextThread()</tt> would
	 *		return.
	 */
	protected ThreadState pickNextThread() {
	    // implement me
	    if (queue.size() != 0) {
		return queue.peek();
	    }
	    else {
		return null;
	    }
	}
	
	public void print() {
	    Lib.assertTrue(Machine.interrupt().disabled());
	    // implement me (if you want)
	}

	/**
	 * helper method of compareTo.
	 */
	protected void updateValue() {
	    if (queue.size() == 0 || !transferPriority) {
		value = 0;
	    }
	    else {
		value = queue.peek().effectivePriority;
	    }
	}

	/**
	 * <tt>true</tt> if this queue should transfer priority from waiting
	 * threads to the owning thread.
	 */
	public boolean transferPriority;
	/**
	 * use java.util.PriorityQueue to store ThreadState in this 
	 * PriorityQueue.
	 */
	protected java.util.PriorityQueue<ThreadState> queue;
	/**
	 * keep track of owner of this PriorityQueue.
	 */
	protected ThreadState owner; 
	/** keep track of the value of this PriorityQueue */
	protected int value;
	/** id of this PriorityQueue, used to break tie */
	protected int id;

    }

    /**
     * A Comparator class that defines how PriorityQueue should be compared
     * in java.util.PriorityQueue. Apply reversed order because java.util.
     * PriorityQueue is a min-heap.
     */
    protected class QueueCompare implements Comparator<PriorityQueue> {
	public int compare(PriorityQueue q1, PriorityQueue q2) {
	    if (q1.value < q2.value) {
		return 1;
	    }
	    else if (q1.value == q2.value) {
		return q1.id-q2.id;

	    }
	    else {
		return -1;
	    }	    
	}
	public boolean equals(Object obj) {
	    if (this == obj) {
		return true;
	    }
	    else {
		return false;
	    }
	}
    }

    /**
     * The scheduling state of a thread. This should include the thread's
     * priority, its effective priority, any objects it owns, and the queue
     * it's waiting for, if any.
     *
     * @see	nachos.threads.KThread#schedulingState
     */
    protected class ThreadState {
	/**
	 * Allocate a new <tt>ThreadState</tt> object and associate it with the
	 * specified thread.
	 *
	 * @param	thread	the thread this state belongs to.
	 */
	public ThreadState(KThread thread) {
	    this.thread = thread;
	    donors = new java.util.PriorityQueue<PriorityQueue>(255, new QueueCompare()); 
	    waiting = null;
	    time = (long) -1;	    
	    setPriority(priorityDefault);
	}

	/**
	 * Return the priority of the associated thread.
	 *
	 * @return	the priority of the associated thread.
	 */
	public int getPriority() {
	    return priority;
	}

	/**
	 * Return the effective priority of the associated thread.
	 *
	 * @return	the effective priority of the associated thread.
	 */
	public int getEffectivePriority() {
	    // implement me
	    return effectivePriority;
	}

	/**
	 * Set the priority of the associated thread to the specified value.
	 *
	 * @param	priority	the new priority.
	 */
	public void setPriority(int priority) {
	    if (this.priority == priority) {
		return;
	    }	    
	    this.priority = priority;	    
	    // implement me
	    if (donors.size() == 0) {
		if (effectivePriority != priority) {
		    updateEffectivePriority(priority);
		}
	    }
	    else {
		if (effectivePriority != Math.max(priority, donors.peek().value)) {
		    updateEffectivePriority(Math.max(priority, donors.peek().value));
		}
	    }		    
	}

	/**
	 * Called when <tt>waitForAccess(thread)</tt> (where <tt>thread</tt> is
	 * the associated thread) is invoked on the specified priority queue.
	 * The associated thread is therefore waiting for access to the
	 * resource guarded by <tt>waitQueue</tt>. This method is only called
	 * if the associated thread cannot immediately obtain access.
	 *
	 * @param	waitQueue	the queue that the associated thread is
	 *				now waiting on.
	 *
	 * @see	nachos.threads.ThreadQueue#waitForAccess
	 */
	public void waitForAccess(PriorityQueue waitQueue)  {
	    // implement me
	    if (waiting == null) {
		waiting = waitQueue;
		time =  Machine.timer().getTime();
		waitQueue.queue.add(this);
		if (effectivePriority > waitQueue.value && waitQueue.transferPriority) {
		    if (waitQueue.owner != null) {
			waitQueue.owner.donors.remove(waitQueue);
			waitQueue.updateValue();
			waitQueue.owner.donors.add(waitQueue);
			if (waitQueue.owner.effectivePriority < waitQueue.value) {
			    waitQueue.owner.updateEffectivePriority(waitQueue.value);
			}
		    }
		    else {
			waitQueue.updateValue();
		    }
		}
	    }
	}

	/**
	 * Called when the associated thread has acquired access to whatever is
	 * guarded by <tt>waitQueue</tt>. This can occur either as a result of
	 * <tt>acquire(thread)</tt> being invoked on <tt>waitQueue</tt> (where
	 * <tt>thread</tt> is the associated thread), or as a result of
	 * <tt>nextThread()</tt> being invoked on <tt>waitQueue</tt>.
	 *
	 * @see	nachos.threads.ThreadQueue#acquire
	 * @see	nachos.threads.ThreadQueue#nextThread
	 */
	public void acquire(PriorityQueue waitQueue) {
	    // implement me
	    waitQueue.owner = this;
	    donors.add(waitQueue);
	}

	/** update effective priority starting from this ThreadState */
	protected void updateEffectivePriority(int newPriority) {
	    if (waiting != null) {
		waiting.queue.remove(this);
		effectivePriority = newPriority;
		waiting.queue.add(this);
		if (waiting.owner != null) {
		    waiting.owner.donors.remove(waiting);
		    waiting.updateValue();
		    waiting.owner.donors.add(waiting);
		    if (waiting.owner.effectivePriority != Math.max(waiting.owner.priority, waiting.owner.donors.peek().value)) {
			waiting.owner.updateEffectivePriority(Math.max(waiting.owner.priority, waiting.owner.donors.peek().value));
		    }
		}
		else {
		    waiting.updateValue();
		}	
	    }
	    else {
		effectivePriority = newPriority;
	    }		
	}

	/** The thread with which this object is associated. */	   
	protected KThread thread;
	/** The priority of the associated thread. */
	protected int priority;
	/** cache effective priority */
	protected int effectivePriority;
	/** keep track of PriorityQueue owned by this ThreadState. */
	protected java.util.PriorityQueue<PriorityQueue> donors;
	/** keep track of PriorityQueue which this ThreadState is in. */
	protected PriorityQueue waiting; 
	/** keep track of when this ThreadState enters its waiting queue. */
	protected long time;
    }
    
    /**
     * A Comparator class that defines how ThreadState should be compared
     * in java.util.PriorityQueue. Apply reversed order because java.util.
     * PriorityQueue is a min-heap.
     */
    protected class ThreadCompare implements Comparator<ThreadState> {
	public int compare(ThreadState t1, ThreadState t2) {	
	    if (t1.getEffectivePriority() < t2.getEffectivePriority()) {
		return 1;
	    }
	    else if (t1.getEffectivePriority() == t2.getEffectivePriority()) {
		return ((Long) t1.time).compareTo((Long) t2.time);
	    }
	    else {
		return -1;
	    }    
	}
	public boolean equals(Object obj) {
	    if (this == obj) {
		return true;
	    }
	    else {
		return false;
	    }
	}
    }    
    
    public static void selfTest() {
	ThreadQueue tq1 = ThreadedKernel.scheduler.newThreadQueue(true), tq2 = ThreadedKernel.scheduler.newThreadQueue(true), tq3 = ThreadedKernel.scheduler.newThreadQueue(true);
	KThread kt_1 = new KThread(), kt_2 = new KThread(), kt_3 = new KThread(), kt_4 = new KThread();

	boolean status = Machine.interrupt().disable();

	tq1.waitForAccess(kt_1);
	tq2.waitForAccess(kt_2);
	tq3.waitForAccess(kt_3);

	tq1.acquire(kt_2);
	tq2.acquire(kt_3);
	tq3.acquire(kt_4);

	ThreadedKernel.scheduler.setPriority(kt_1, 6);

	Lib.assertTrue(ThreadedKernel.scheduler.getEffectivePriority(kt_4)==6);

	KThread kt_5 = new KThread();

	ThreadedKernel.scheduler.setPriority(kt_5, 7);

	tq1.waitForAccess(kt_5);

	Lib.assertTrue(ThreadedKernel.scheduler.getEffectivePriority(kt_4)==7);

	tq1.nextThread();

	Lib.assertTrue(ThreadedKernel.scheduler.getEffectivePriority(kt_4)==1);

	Machine.interrupt().restore(status);
    }


    public static void test1() {
	PriorityQueue q1 = (PriorityQueue) ThreadedKernel.scheduler.newThreadQueue(true);
	KThread t1 = new KThread();
	boolean status = Machine.interrupt().disable();
	ThreadState ts1 = ((PriorityScheduler) ThreadedKernel.scheduler).getThreadState(t1);
	q1.acquire(t1);
	Lib.assertTrue(q1.owner==ts1);
	Lib.assertTrue(ts1.donors.peek()==q1);
	Lib.assertTrue(ts1.effectivePriority==1);
	Machine.interrupt().restore(status);	
    }
    
    public static void test2() {
	KThread t1 = new KThread();
	boolean status = Machine.interrupt().disable();
	((PriorityScheduler) ThreadedKernel.scheduler).setPriority(t1, 3);
	ThreadState ts1 = ((PriorityScheduler) ThreadedKernel.scheduler).getThreadState(t1);	
	Lib.assertTrue(ts1.priority==3);
	Lib.assertTrue(ts1.effectivePriority==3);	
	Machine.interrupt().restore(status);
    }

    public static void test3() {
	PriorityQueue q1 = (PriorityQueue) ThreadedKernel.scheduler.newThreadQueue(true);
	KThread t1 = new KThread();
	KThread t2 = new KThread();
	boolean status = Machine.interrupt().disable();
	q1.acquire(t1);
	ThreadState ts1 = ((PriorityScheduler) ThreadedKernel.scheduler).getThreadState(t1);
	((PriorityScheduler) ThreadedKernel.scheduler).setPriority(t2, 4);
	ThreadState ts2 = ((PriorityScheduler) ThreadedKernel.scheduler).getThreadState(t2);	
	q1.waitForAccess(t2);
	Lib.assertTrue(q1.queue.size()==1);
	Lib.assertTrue(q1.queue.peek()==ts2);
	Lib.assertTrue(q1.value==4);
	Lib.assertTrue(ts1.effectivePriority==4);
	Machine.interrupt().restore(status);
    }
  
    public static void test4() {
	PriorityQueue q1 = (PriorityQueue) ThreadedKernel.scheduler.newThreadQueue(true);
	PriorityQueue q2 = (PriorityQueue) ThreadedKernel.scheduler.newThreadQueue(false);
	KThread t1 = new KThread();
	KThread t2 = new KThread();
	KThread t3 = new KThread();
	KThread t4 = new KThread();
	KThread t5 = new KThread();
	KThread t6 = new KThread();
	KThread t7 = new KThread();
	KThread t8 = new KThread();
	KThread t9 = new KThread();
	KThread t10 = new KThread();
	boolean status = Machine.interrupt().disable();
	q1.waitForAccess(t1);	
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	q1.waitForAccess(t2);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	q1.waitForAccess(t3);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	q1.waitForAccess(t7);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	q1.waitForAccess(t8);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	q1.waitForAccess(t9);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	q1.waitForAccess(t10);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	Lib.assertTrue(q1.nextThread()==t1);
	Lib.assertTrue(q1.owner.thread==t1);	
	Lib.assertTrue(q1.nextThread()==t2);
	Lib.assertTrue(q1.owner.thread==t2);	
	Lib.assertTrue(q1.nextThread()==t3);
	Lib.assertTrue(q1.owner.thread==t3);
	Lib.assertTrue(q1.nextThread()==t7);
	Lib.assertTrue(q1.owner.thread==t7);
	Lib.assertTrue(q1.nextThread()==t8);
	Lib.assertTrue(q1.owner.thread==t8);
	Lib.assertTrue(q1.nextThread()==t9);
	Lib.assertTrue(q1.owner.thread==t9);
	Lib.assertTrue(q1.nextThread()==t10);
	Lib.assertTrue(q1.owner.thread==t10);
	Lib.assertTrue(q1.nextThread()==null);
	Lib.assertTrue(q1.owner==null);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	q2.acquire(t1);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	q2.waitForAccess(t4);	
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	q2.waitForAccess(t5);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	q2.waitForAccess(t6);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	Lib.assertTrue(q2.owner.thread==t1);
	Lib.assertTrue(q2.nextThread()==t4);
	Lib.assertTrue(q2.owner.thread==t4);
	Lib.assertTrue(q2.nextThread()==t5);
	Lib.assertTrue(q2.owner.thread==t5);
	Lib.assertTrue(q2.nextThread()==t6);
	Lib.assertTrue(q2.owner.thread==t6);
	Lib.assertTrue(q2.nextThread()==null);
	Lib.assertTrue(q2.owner==null);
	Machine.interrupt().restore(status);
    }

    public static void test5() {
	PriorityQueue q1 = (PriorityQueue) ThreadedKernel.scheduler.newThreadQueue(true);
	KThread t1 = new KThread();
	KThread t2 = new KThread();
	boolean status = Machine.interrupt().disable();
	q1.acquire(t1);
	ThreadState ts1 = ((PriorityScheduler) ThreadedKernel.scheduler).getThreadState(t1);	
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	((PriorityScheduler) ThreadedKernel.scheduler).setPriority(t2, 4);
	ThreadState ts2 = ((PriorityScheduler) ThreadedKernel.scheduler).getThreadState(t2);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	q1.waitForAccess(t2);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	Lib.assertTrue(q1.nextThread()==t2);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	Lib.assertTrue(ts1.effectivePriority==1);
	Lib.assertTrue(q1.owner==ts2);
	Lib.assertTrue(ts2.donors.peek()==q1);
	Lib.assertTrue(q1.queue.size()==0);
	Lib.assertTrue(q1.value==0);
	Lib.assertTrue(ts2.waiting==null);
	Machine.interrupt().restore(status);
    }

    public static void test6() {
	PriorityQueue q1 = (PriorityQueue) ThreadedKernel.scheduler.newThreadQueue(true);
	KThread t1 = new KThread();
	KThread t2 = new KThread();
	KThread t3 = new KThread();
	KThread t4 = new KThread();
	boolean status = Machine.interrupt().disable();
	q1.acquire(t1);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();	
	q1.waitForAccess(t2);	
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	q1.waitForAccess(t3);	
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	q1.waitForAccess(t4);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	((PriorityScheduler) ThreadedKernel.scheduler).setPriority(t2, 4);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	((PriorityScheduler) ThreadedKernel.scheduler).setPriority(t3, 5);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	((PriorityScheduler) ThreadedKernel.scheduler).setPriority(t4, 3);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	Lib.assertTrue(q1.value==5);
	Lib.assertTrue(((PriorityScheduler) ThreadedKernel.scheduler).getEffectivePriority(t1)==5);
	Machine.interrupt().restore(status);
    }

    public static void test7() {
	PriorityQueue q1 = (PriorityQueue) ThreadedKernel.scheduler.newThreadQueue(true);
	PriorityQueue q2 = (PriorityQueue) ThreadedKernel.scheduler.newThreadQueue(true);
	PriorityQueue q3 = (PriorityQueue) ThreadedKernel.scheduler.newThreadQueue(true);
	KThread t1 = new KThread();
	KThread t2 = new KThread();
	KThread t3 = new KThread();
	KThread t4 = new KThread();
	boolean status = Machine.interrupt().disable();
	q1.acquire(t1);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	q2.acquire(t1);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	q3.acquire(t1);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	((PriorityScheduler) ThreadedKernel.scheduler).setPriority(t2, 4);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	q1.waitForAccess(t2);	
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	((PriorityScheduler) ThreadedKernel.scheduler).setPriority(t3, 5);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	q2.waitForAccess(t3);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();	
	((PriorityScheduler) ThreadedKernel.scheduler).setPriority(t4, 3);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	q3.waitForAccess(t4);	
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	Lib.assertTrue(((PriorityScheduler) ThreadedKernel.scheduler).getEffectivePriority(t1)==5);
	Machine.interrupt().restore(status);
    }

    public static void test8() {
	PriorityQueue q1 = (PriorityQueue) ThreadedKernel.scheduler.newThreadQueue(true);
	PriorityQueue q2 = (PriorityQueue) ThreadedKernel.scheduler.newThreadQueue(true);
	KThread t1 = new KThread();
	KThread t2 = new KThread();
	KThread t3 = new KThread();
	boolean status = Machine.interrupt().disable();
	q1.acquire(t1);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	((PriorityScheduler) ThreadedKernel.scheduler).setPriority(t2, 4);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	q1.waitForAccess(t2);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	q2.acquire(t2);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	((PriorityScheduler) ThreadedKernel.scheduler).setPriority(t3, 5);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();	
	q2.waitForAccess(t3);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	Lib.assertTrue(q1.value==5);
	Lib.assertTrue(((PriorityScheduler) ThreadedKernel.scheduler).getEffectivePriority(t1)==5);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	((PriorityScheduler) ThreadedKernel.scheduler).setPriority(t3, 3);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	Lib.assertTrue(q1.value==4);
	Lib.assertTrue(((PriorityScheduler) ThreadedKernel.scheduler).getEffectivePriority(t1)==4);	
	Machine.interrupt().restore(status);
    }
    
    public static void test9() {
	PriorityQueue q1 = (PriorityQueue) ThreadedKernel.scheduler.newThreadQueue(false);
	PriorityQueue q2 = (PriorityQueue) ThreadedKernel.scheduler.newThreadQueue(false);
	KThread t1 = new KThread();
	KThread t2 = new KThread();
	KThread t3 = new KThread();	
	KThread t4 = new KThread();
	KThread t5 = new KThread();
	boolean status = Machine.interrupt().disable();
	ThreadedKernel.scheduler.setPriority(t2, 2);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	ThreadedKernel.scheduler.setPriority(t3, 3);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	ThreadedKernel.scheduler.setPriority(t4, 4);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	ThreadedKernel.scheduler.setPriority(t5, 5);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	q1.acquire(t1);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	q1.waitForAccess(t2);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	q1.waitForAccess(t3);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	q2.acquire(t2);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	q2.waitForAccess(t4);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	Lib.assertTrue(q1.nextThread()==t3);
	Lib.assertTrue(q1.owner.thread==t3);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	q2.waitForAccess(t5);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	q2.waitForAccess(t1);
	Machine.interrupt().restore(status);
	status = Machine.interrupt().disable();
	Lib.assertTrue(q2.nextThread()==t5);
	Lib.assertTrue(q2.owner.thread==t5);
	Lib.assertTrue(q1.nextThread()==t2);
	Lib.assertTrue(q1.owner.thread==t2);
	Lib.assertTrue(q2.nextThread()==t4);
	Lib.assertTrue(q2.owner.thread==t4);
	Lib.assertTrue(q2.nextThread()==t1);
	Lib.assertTrue(q2.owner.thread==t1);	
	Machine.interrupt().restore(status);
    }
}
