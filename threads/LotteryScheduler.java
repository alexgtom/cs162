package nachos.threads;

import nachos.machine.*;

import java.util.HashSet;
import java.util.Iterator;


/**
 * A scheduler that chooses threads using a lottery.
 *
 * <p>
 * A lottery scheduler associates a number of tickets with each thread. When a
 * thread needs to be dequeued, a random lottery is held, among all the tickets
 * of all the threads waiting to be dequeued. The thread that holds the winning
 * ticket is chosen.
 *
 * <p>
 * Note that a lottery scheduler must be able to handle a lot of tickets
 * (sometimes billions), so it is not acceptable to maintain state for every
 * ticket.
 *
 * <p>
 * A lottery scheduler must partially solve the priority inversion problem; in
 * particular, tickets must be transferred through locks, and through joins.
 * Unlike a priority scheduler, these tickets add (as opposed to just taking
 * the maximum).
 */
public class LotteryScheduler extends PriorityScheduler {
    /**
     * Allocate a new lottery scheduler.
     */
    public LotteryScheduler() {
    }
    
    /**
     * The minimum priority that a thread can have. Do not change this value.
     */
    public static final int priorityMinimum = 1;
    /**
     * The maximum priority that a thread can have. Do not change this value.
     */
    public static final int priorityMaximum = Integer.MAX_VALUE; 
    /**
     * Total tickets assigned in the system
     */
    protected static int total = 0;

    /**
     * Allocate a new lottery thread queue.
     *
     * @param	transferPriority	<tt>true</tt> if this queue should
     *					transfer tickets from waiting threads
     *					to the owning thread.
     * @return	a new lottery thread queue.
     */
    @Override
    public ThreadQueue newThreadQueue(boolean transferPriority) {
  	ThreadQueue queue =  new LotteryQueue(transferPriority);
	return queue;
    }

    @Override
    public void setPriority(KThread thread, int priority) {
	Lib.assertTrue(Machine.interrupt().disabled());
		       
	Lib.assertTrue(priority >= priorityMinimum &&
		   priority <= priorityMaximum);
	
	getThreadState(thread).setPriority(priority);
    }

    @Override
    public boolean increasePriority() {
	boolean intStatus = Machine.interrupt().disable();
		       
	KThread thread = KThread.currentThread();

	int priority = getPriority(thread);
	if (priority == priorityMaximum)
	    return false;

	setPriority(thread, priority+1);

	Machine.interrupt().restore(intStatus);
	return true;
    }

    @Override
    public boolean decreasePriority() {
	boolean intStatus = Machine.interrupt().disable();
		       
	KThread thread = KThread.currentThread();

	int priority = getPriority(thread);
	if (priority == priorityMinimum)
	    return false;

	setPriority(thread, priority-1);

	Machine.interrupt().restore(intStatus);
	return true;
    }

    /**
     * Return the scheduling state of the specified thread.
     *
     * @param	thread	the thread whose scheduling state to return.
     * @return	the scheduling state of the specified thread.
     */
    @Override
    protected ThreadState getThreadState(KThread thread) {
	if (thread.schedulingState == null)
	    thread.schedulingState = new LotteryState(thread);

	return (ThreadState) thread.schedulingState;
    }

    protected class LotteryQueue extends PriorityQueue {

	// number of tickets in the queue
	protected int sum;
	// a set of lottery states waiting on this queue
	protected HashSet<LotteryState> pool;
	// holder of the resource
	protected LotteryState holder;

	
 	public LotteryQueue(boolean transferPriority) {
	    super(transferPriority);
	    sum = 0;
	    pool = new HashSet<LotteryState>();
	    holder = null;
	}

	@Override
	public void waitForAccess(KThread thread) {
	    Lib.assertTrue(Machine.interrupt().disabled());
	    getThreadState(thread).waitForAccess(this);
	}

	@Override
	public void acquire(KThread thread) {
	    Lib.assertTrue(Machine.interrupt().disabled());
	    getThreadState(thread).acquire(this);
	}

	@Override
	public KThread nextThread() {
	    Lib.assertTrue(Machine.interrupt().disabled());
	    if (pool.size() != 0) {
		if (holder != null) {
		    LotteryState old = holder; 
		    holder = null;
		    if (transferPriority) {
			old.updateEffectiveTickets(old.eTickets - sum);
		    }
		}
		LotteryState winner = (LotteryState) pickNextThread();
		pool.remove(winner);
		winner.waiton = null;
		sum -= winner.eTickets;
		winner.acquire(this);
		return winner.thread;
	    }
	    else {
		if (holder != null) {
		    holder = null;
		}
		return null;
	    }		
	}

	/**
	 * Return the next thread that <tt>nextThread()</tt> would return,
	 * without modifying the state of this queue.
	 *
	 * @return	the next thread that <tt>nextThread()</tt> would
	 *		return.
	 */
	@Override
	protected ThreadState pickNextThread() {
	    if (pool.size() != 0) {
		int rand = (int) (Math.random() * sum);
		Iterator<LotteryState> iter = pool.iterator();
		LotteryState winner = null;
		while (rand >= 0) {
		    winner = iter.next();
		    rand -= winner.eTickets;
		}
		return winner;
	    }
	    else {
		return null;
	    }
	}

	@Override
	public void print() {
	    Lib.assertTrue(Machine.interrupt().disabled());
	    // implement me (if you want)
	}


    }

    protected class LotteryState extends ThreadState {
	// number of tickets assigned by scheduler
	protected int tickets;
	// cached effective number of tickets
	protected int eTickets;
	// a lottery queue this thread is waiting for
	protected LotteryQueue waiton;


	/**
	 * Allocate a new <tt>ThreadState</tt> object and associate it with the
	 * specified thread.
	 *
	 * @param	thread	the thread this state belongs to.
	 */
	public LotteryState(KThread thread) {
	    super(thread);
	    tickets = 0;
	    eTickets = 0;
	    waiton = null;
	    setPriority(priorityDefault);
	}

	/**
	 * Return the number of tickets of the associated thread.
	 *
	 * @return	the number of tickets of the associated thread.
	 */
	@Override
	public int getPriority() {
	    return tickets;
	}

	/**
	 * Return the effective number of tickets of the associated thread.
	 *
	 * @return the effective number of tickets of the associated thread.
	 */
	@Override
	public int getEffectivePriority() {
	    return eTickets;
	}

	/**
	 * Set the tickets of the associated thread to the specified value.
	 *
	 * @param	priority	the new priority.
	 */
	@Override
	public void setPriority(int priority) {
	    if (tickets == priority) {
		return;
	    }
	    int diff = priority - tickets;
	    if (diff > 0) {
		Lib.assertTrue(total + diff > total);
	    }
	    tickets += diff;
	    total += diff;
	    updateEffectiveTickets(eTickets + diff);
	}

	/**
	 * Called when <tt>waitForAccess(thread)</tt> (where <tt>thread</tt> is
	 * the associated thread) is invoked on the specified priority queue.
	 * The associated thread is therefore waiting for access to the
	 * resource guarded by <tt>waitQueue</tt>. This method is only called
	 * if the associated thread cannot immediately obtain access.
	 *
	 * @param	waitQueue	the queue that the associated thread is
	 *				now waiting on.
	 *
	 * @see	nachos.threads.ThreadQueue#waitForAccess
	 */
	@Override
	public void waitForAccess(PriorityQueue waitQueue)  {
	    LotteryQueue wait = (LotteryQueue) waitQueue;
	    if (waiton == null) {
		waiton = wait;
		wait.pool.add(this);
		wait.sum += eTickets;
		if (wait.transferPriority && wait.holder != null) {
		    wait.holder.updateEffectiveTickets(wait.holder.eTickets + eTickets);
		}
	    }
	}

	/**
	 * Called when the associated thread has acquired access to whatever is
	 * guarded by <tt>waitQueue</tt>. This can occur either as a result of
	 * <tt>acquire(thread)</tt> being invoked on <tt>waitQueue</tt> (where
	 * <tt>thread</tt> is the associated thread), or as a result of
	 * <tt>nextThread()</tt> being invoked on <tt>waitQueue</tt>.
	 *
	 * @see	nachos.threads.ThreadQueue#acquire
	 * @see	nachos.threads.ThreadQueue#nextThread
	 */
	@Override
	public void acquire(PriorityQueue waitQueue) {
	    LotteryQueue wait = (LotteryQueue) waitQueue;
	    wait.holder = this;
	    if (wait.transferPriority) {
		updateEffectiveTickets(eTickets + wait.sum);
	    }
	}

	/** update effective priority starting from this ThreadState */
	protected void updateEffectiveTickets(int t) {
	    int diff = t - eTickets;
	    eTickets += diff;
	    if (waiton != null) {
		waiton.sum += diff;
		if (waiton.holder != null && waiton.transferPriority) {
		    waiton.holder.updateEffectiveTickets(waiton.holder.eTickets + diff);
		}
	    }
	}	
    }
    
    public static void test1() {
    	LotteryQueue q1 = (LotteryQueue) ThreadedKernel.scheduler.newThreadQueue(true);
    	KThread t1 = new KThread();
    	boolean status = Machine.interrupt().disable();
    	ThreadState ts1 = ((LotteryScheduler) ThreadedKernel.scheduler).getThreadState(t1);
    	q1.acquire(t1);
    	Lib.assertTrue(q1.holder==ts1);
    	Lib.assertTrue(ts1.getEffectivePriority()==1);
    	Lib.assertTrue(ts1.getPriority()==1);
    	Machine.interrupt().restore(status);
    }
    
    public static void test2() {
    	KThread t1 = new KThread();
    	boolean status = Machine.interrupt().disable();
    	((LotteryScheduler) ThreadedKernel.scheduler).setPriority(t1, 100);
    	ThreadState ts1 = ((LotteryScheduler) ThreadedKernel.scheduler).getThreadState(t1);	
    	Lib.assertTrue(ts1.getPriority()==100);
    	Lib.assertTrue(ts1.getEffectivePriority()==100);	
    	Machine.interrupt().restore(status);
    }
    
    public static void test3() {
    	LotteryQueue q1 = (LotteryQueue) ThreadedKernel.scheduler.newThreadQueue(true);
    	KThread t1 = new KThread();
    	KThread t2 = new KThread();
    	boolean status = Machine.interrupt().disable();
    	q1.acquire(t1);
    	ThreadState ts1 = ((LotteryScheduler) ThreadedKernel.scheduler).getThreadState(t1);
		LotteryState ts2 = (LotteryState) ((LotteryScheduler) ThreadedKernel.scheduler).getThreadState(t2);	
		q1.waitForAccess(t2);
		Lib.assertTrue(q1.pool.size()==1);
		Lib.assertTrue(ts2.waiton==q1);
		Lib.assertTrue(q1.sum==1);
		Lib.assertTrue(ts1.getEffectivePriority()==2);
		Machine.interrupt().restore(status);
    }
    
    public static void test4() {
    	LotteryQueue q1 = (LotteryQueue) ThreadedKernel.scheduler.newThreadQueue(true);
    	KThread t1 = new KThread();
    	KThread t2 = new KThread();
    	boolean status = Machine.interrupt().disable();
		q1.acquire(t1);
		ThreadState ts1 = ((LotteryScheduler) ThreadedKernel.scheduler).getThreadState(t1);	
		LotteryState ts2 = (LotteryState) ((LotteryScheduler) ThreadedKernel.scheduler).getThreadState(t2);
		Machine.interrupt().restore(status);
		status = Machine.interrupt().disable();
		q1.waitForAccess(t2);
		Machine.interrupt().restore(status);
		status = Machine.interrupt().disable();
		Lib.assertTrue(q1.nextThread()==t2);
		Machine.interrupt().restore(status);
		status = Machine.interrupt().disable();
		Lib.assertTrue(ts1.getEffectivePriority()==1);
		Lib.assertTrue(q1.holder==ts2);
		Lib.assertTrue(q1.pool.size()==0);
		Lib.assertTrue(q1.sum==0);
		Lib.assertTrue(ts2.waiton==null);
		Machine.interrupt().restore(status);
    }
    
    public static void test5() {
    	LotteryQueue q1 = (LotteryQueue) ThreadedKernel.scheduler.newThreadQueue(true);
    	LotteryQueue q2 = (LotteryQueue) ThreadedKernel.scheduler.newThreadQueue(true);
    	LotteryQueue q3 = (LotteryQueue) ThreadedKernel.scheduler.newThreadQueue(true);
    	KThread t1 = new KThread();
    	KThread t2 = new KThread();
    	KThread t3 = new KThread();
		KThread t4 = new KThread();
		boolean status = Machine.interrupt().disable();
		q1.acquire(t1);
		Machine.interrupt().restore(status);
		status = Machine.interrupt().disable();
		q2.acquire(t1);
		Machine.interrupt().restore(status);
		status = Machine.interrupt().disable();
		q3.acquire(t1);
		Machine.interrupt().restore(status);
		status = Machine.interrupt().disable();
		((LotteryScheduler) ThreadedKernel.scheduler).setPriority(t2, 2);
		Machine.interrupt().restore(status);
		status = Machine.interrupt().disable();
		q1.waitForAccess(t2);	
		Machine.interrupt().restore(status);
		status = Machine.interrupt().disable();
		((LotteryScheduler) ThreadedKernel.scheduler).setPriority(t3, 3);
		Machine.interrupt().restore(status);
		status = Machine.interrupt().disable();
		q2.waitForAccess(t3);
		Machine.interrupt().restore(status);
		status = Machine.interrupt().disable();	
		((LotteryScheduler) ThreadedKernel.scheduler).setPriority(t4, 4);
		Machine.interrupt().restore(status);
		status = Machine.interrupt().disable();
		q3.waitForAccess(t4);	
		Machine.interrupt().restore(status);
		status = Machine.interrupt().disable();
		Lib.assertTrue(((LotteryScheduler) ThreadedKernel.scheduler).getEffectivePriority(t1)==10);
		Machine.interrupt().restore(status);
    } 
    
    public static void test6() {
    	LotteryQueue q1 = (LotteryQueue) ThreadedKernel.scheduler.newThreadQueue(true);
    	KThread t1 = new KThread();
    	KThread t2 = new KThread();
    	KThread t3 = new KThread();
    	KThread t4 = new KThread();
    	boolean status = Machine.interrupt().disable();
    	q1.acquire(t1);
    	Machine.interrupt().restore(status);
    	status = Machine.interrupt().disable();
    	((LotteryScheduler) ThreadedKernel.scheduler).setPriority(t2, 2);
    	Machine.interrupt().restore(status);
		status = Machine.interrupt().disable();	
		q1.waitForAccess(t2);	
		Machine.interrupt().restore(status);
		status = Machine.interrupt().disable();
		((LotteryScheduler) ThreadedKernel.scheduler).setPriority(t3, 3);
		Machine.interrupt().restore(status);
		status = Machine.interrupt().disable();
		q1.waitForAccess(t3);	
		Machine.interrupt().restore(status);
		status = Machine.interrupt().disable();
		((LotteryScheduler) ThreadedKernel.scheduler).setPriority(t4, 4);
		Machine.interrupt().restore(status);
		status = Machine.interrupt().disable();
		q1.waitForAccess(t4);
		Machine.interrupt().restore(status);
		status = Machine.interrupt().disable();
		Lib.assertTrue(q1.sum==9);
		Lib.assertTrue(((LotteryScheduler) ThreadedKernel.scheduler).getEffectivePriority(t1)==10);
		Machine.interrupt().restore(status);
    }
    
    public static void test7() {
    	LotteryQueue q1 = (LotteryQueue) ThreadedKernel.scheduler.newThreadQueue(true);
    	LotteryQueue q2 = (LotteryQueue) ThreadedKernel.scheduler.newThreadQueue(true);
    	KThread t1 = new KThread();
    	KThread t2 = new KThread();
    	KThread t3 = new KThread();
    	boolean status = Machine.interrupt().disable();
    	q1.acquire(t1);
    	Machine.interrupt().restore(status);
    	status = Machine.interrupt().disable();
    	((LotteryScheduler) ThreadedKernel.scheduler).setPriority(t2, 4);
    	Machine.interrupt().restore(status);
    	status = Machine.interrupt().disable();
    	q1.waitForAccess(t2);
    	Machine.interrupt().restore(status);
    	status = Machine.interrupt().disable();
    	q2.acquire(t2);
    	Machine.interrupt().restore(status);
    	status = Machine.interrupt().disable();
    	((LotteryScheduler) ThreadedKernel.scheduler).setPriority(t3, 5);
    	Machine.interrupt().restore(status);
    	status = Machine.interrupt().disable();	
    	q2.waitForAccess(t3);
    	Machine.interrupt().restore(status);
    	status = Machine.interrupt().disable();
    	Lib.assertTrue(q1.sum==9);
    	Lib.assertTrue(((LotteryScheduler) ThreadedKernel.scheduler).getEffectivePriority(t1)==10);
    	Machine.interrupt().restore(status);
    }
    
    public static void test8() {
    	LotteryQueue q1 = (LotteryQueue) ThreadedKernel.scheduler.newThreadQueue(true);
    	KThread t1 = new KThread();
    	KThread t2 = new KThread();
    	boolean status = Machine.interrupt().disable();
    	q1.acquire(t1);
    	ThreadState ts1 = ((LotteryScheduler) ThreadedKernel.scheduler).getThreadState(t1);
		LotteryState ts2 = (LotteryState) ((LotteryScheduler) ThreadedKernel.scheduler).getThreadState(t2);	
		q1.waitForAccess(t2);
    	((LotteryScheduler) ThreadedKernel.scheduler).setPriority(t2, 2);	
		Machine.interrupt().restore(status);   
		Lib.assertTrue(q1.sum==2);
		Lib.assertTrue(ts1.getEffectivePriority()==3);
    }

    public static void test9() {
    	LotteryQueue q1 = (LotteryQueue) ThreadedKernel.scheduler.newThreadQueue(false);
    	KThread t1 = new KThread();
    	KThread t2 = new KThread();
    	boolean status = Machine.interrupt().disable();
    	q1.acquire(t1);
    	ThreadState ts1 = ((LotteryScheduler) ThreadedKernel.scheduler).getThreadState(t1);
	LotteryState ts2 = (LotteryState) ((LotteryScheduler) ThreadedKernel.scheduler).getThreadState(t2);	
	q1.waitForAccess(t2);
	Lib.assertTrue(q1.pool.size()==1);
	Lib.assertTrue(ts2.waiton==q1);
	Lib.assertTrue(q1.sum==1);
	Lib.assertTrue(ts1.getEffectivePriority()==1);
	Machine.interrupt().restore(status);
    }
}
