package nachos.threads;
import nachos.ag.BoatGrader;

public class Island {
    public int numAdults = 0;
    public int numChildren = 0;
    public String name;
    static Lock countLock = new Lock();

    public Island(String name) {
        this.name = name;
    }
    
    public int incNumAdults() {
        return incNumAdults(1);
    }

    public int decNumAdults() {
        return decNumAdults(1);
    }

    public int incNumAdults(int i) {
        countLock.acquire();
        numAdults += i; 
        countLock.release();
        return numAdults;
    }

    public int decNumAdults(int i) {
        countLock.acquire();
        numAdults -= i; 
        countLock.release();
        return numAdults;
    }
    
    public int incNumChildren() {
        return incNumChildren(1);
    }

    public int decNumChildren() {
        return decNumChildren(1);
    }

    public int incNumChildren(int i) {
        countLock.acquire();
        numChildren += i; 
        countLock.release();
        return numChildren;
    }

    public int decNumChildren(int i) {
        countLock.acquire();
        numChildren -= i; 
        countLock.release();
        return numChildren;
    }

    public int getNumChildren() {
        int t;
        countLock.acquire();
        t = numChildren;
        countLock.release();
        return t; 
    }
    public int getNumAdults() {
        int t;
        countLock.acquire();
        t = numAdults; 
        countLock.release();
        return t; 
    }
    
    public boolean equals(Object other) {
        return ((Island) other).name == name;
    }
    
    public String toString() {
    	return name;
    }

    public void debug() {
        System.err.println("Island: " + name + "  NumChildren:"  + getNumChildren() + 
                "  NumAdults: " + getNumAdults()); 
    }
}
