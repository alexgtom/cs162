package nachos.threads;

import nachos.machine.*;
import java.util.ArrayList;

/**
 * Uses the hardware timer to provide preemption, and to allow threads to sleep
 * until a certain time.
 */
public class Alarm {
    /**
     * Allocate a new Alarm. Set the machine's timer interrupt handler to this
     * alarm's callback.
     *
     * <p><b>Note</b>: Nachos will not function correctly with more than one
     * alarm.
     */

    /**
    * A Min Heap to stores the threads with waking up time as the key.
    */
    private minHeap alarmHeap;

    public Alarm() {
    	alarmHeap = new minHeap();
		Machine.timer().setInterruptHandler(new Runnable() {
			public void run() { timerInterrupt(); }
	    });
    }

    /**
     * The timer interrupt handler. This is called by the machine's timer
     * periodically (approximately every 500 clock ticks). Causes the current
     * thread to yield, forcing a context switch if there is another thread
     * that should be run.
     */
    public void timerInterrupt() {
        if(!alarmHeap.empty()) {
            while(alarmHeap.firstKey() <= Machine.timer().getTime()){
                KThread wakeThread;
                boolean status = Machine.interrupt().disable();
                long key = alarmHeap.firstKey();
                wakeThread = (KThread) alarmHeap.removeFirst();
                wakeThread.ready();
                if(alarmHeap.empty()) {
                    break;
                }
                Machine.interrupt().restore(status);
            }
        }
        KThread.currentThread().yield();
    }

    /**
     * Put the current thread to sleep for at least <i>x</i> ticks,
     * waking it up in the timer interrupt handler. The thread must be
     * woken up (placed in the scheduler ready set) during the first timer
     * interrupt where
     *
     * <p><blockquote>
     * (current time) >= (WaitUntil called time)+(x)
     * </blockquote>
     *
     * @param	x	the minimum number of clock ticks to wait.
     *
     * @see	nachos.machine.Timer#getTime()
     */
    public void waitUntil(long x) {
	// for now, cheat just to get something working (busy waiting is bad)
        boolean status = Machine.interrupt().disable();

        if(x == 0) {
            Machine.interrupt().restore(status);
            return;
        }

        long wakeTime = Machine.timer().getTime() + x;
        KThread currentT = KThread.currentThread();
        alarmHeap.add(wakeTime,currentT);
        KThread.currentThread().sleep();
        Machine.interrupt().restore(status);
    }

    /**
    * The implemention of the min Heap with methods firstKey, add, removeFirst, and empty.
    * firstKey will give you the minimal key of the heap.
    * add will add a object (thread) with a long number as the key into the min heap.
    * removeFirst will remove the object with the smallest long number as its key.
    * empty will return true if the min heap is empty.
    */
    public class minHeap {
        private long key[];
        private int n;
        private Object thread[];

    	public minHeap() {
    		n = -1;
    		key = new long[1024];
    		thread = new Object[1024];
    	}
    	public long firstKey() {
    		if (n>= 0) {
    			return key[0];
    		} else {
    			return -1;
    		}
    	}
    	public void add(long keyarg, Object threadarg) {
    		int allocated = key.length;
    		if (n+1 >= allocated) {
    			allocated *=2;
    			long newKey[] = new long[allocated];
    			Object newThread[] = new Object[allocated];
    			System.arraycopy(key, 0, newKey,0,n);
    			System.arraycopy(thread,0,newThread,0,n);
    			key = newKey;
    			thread = newThread;
    		}
    		n++;
    		int i = n;
    		while (i>0) {
    			if (keyarg >= key[i/2]) {
    				break;
    			}
    			key[i]=key[i/2];
    			thread[i] = thread[i/2];
    			i /=2;
    		}
    		thread[i]=threadarg;
    		key[i]=keyarg;
    	}
    	public Object removeFirst() {
    		Object x = thread[0];
    		long k = key[n];
    		int i = 0, j=1;
    		n--;
    		for(;j<=n;) {
    			if (j<n) {
    				if (key[j]>key[j+1]) {
    					j++;
    				}
    			}
    			if (k<=key[j]){
    				break;
    			}
    			key[i]=key[j];
    			thread[i]=thread[j];
    			i=j;
    			j*=2;
    		}
    		key[i]=key[n+1];
    		thread[i]=thread[n+1];
    		return x;
    	}
    	public boolean empty() {
            return n+1 <= 0;
    	}
    }
}
