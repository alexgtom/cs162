#include "syscall.h"
#include "stdlib.h"
#include "stdio.h"

#define TEST_FILE "test.txt"
#define TEST_TEXT "abcdefg"

void exit_wrapper(int error_code) {
    printf("Failed at %d\n", error_code);
    exit(error_code);
}

void readWriteFileTest() {
    int fp;
    int buffer_size = strlen(TEST_TEXT);
    int count;
    char buffer[buffer_size]; 

    if ((fp = creat(TEST_FILE)) < 0)
        exit_wrapper(1);

    if ((count = write(fp, TEST_TEXT, strlen(TEST_TEXT))) != buffer_size)
    {
        printf("Expected %d but got %d instead.\n", buffer_size, count);
        exit_wrapper(2);
    }

    if (close(fp) < 0)
        exit_wrapper(3);

    if ((fp = open(TEST_FILE)) < 0)
        exit_wrapper(1);

    if ((count = read(fp, buffer, buffer_size)) != buffer_size)
    {
        printf("Expected %d but got %d instead.\n", buffer_size, count);
        exit_wrapper(4);
    }

    if (strcmp(TEST_TEXT, buffer) != 0)        
        exit_wrapper(5);

    if (close(fp) < 0)
        exit_wrapper(6);

    if (unlink(TEST_FILE) < 0)
        exit_wrapper(7);
}

void execJoinTest() {
    int pid;
    if ((pid = exec("sort.coff", 0, 0)) < 0)
        exit_wrapper(8);

    int exit_status;
    if (join(pid, &exit_status) < 0)
        exit_wrapper(9);

    if (exit_status < 0)
        exit_wrapper(10);
    
}

void invalidExec() {
    if (exec("invalidFileName", 0, 0) > -1)
        exit_wrapper(14);
    if (exec(0, 0, 0) > -1)
        exit_wrapper(15);
}

void execJoinTest2() {
    char *file = "echo.coff";
    char *argv[] = { file };
    int childpid;
    if ((childpid = exec(file, 1, argv))<0)
        exit_wrapper(11);
    int exit_status;
    if (join(childpid, &exit_status) != 1)
	 exit_wrapper(12);
    if (exit_status < 0)
        exit_wrapper(13);

    
}

int
main()
{
    printf("---- Beginning Syscall Test ----\n");
    readWriteFileTest();
    execJoinTest();
    execJoinTest2();    
    invalidExec();
    printf("---- End Syscall Test ----\n");

    exit(0);
}
